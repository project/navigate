
var NavigateFavorites = NavigateFavorites || {};

Drupal.behaviors.navigate_favorites = function () {
  
  // Add sortable
  $(".navigate-favorites-list-sortable").each(function () {
    if ($(this).hasClass('favorites-sortable-processed')) {
      return;
    }
    $(this).addClass('favorites-sortable-processed');
    $(this).sortable({
      cursor: "move",
      revert:true,
      distance: 4,
      helper:"clone",
      stop:function(e, ui) {
        var wid = $(this).parents('.navigate-widget').children('.wid').val();
        var query_array = new Array();
        query_array['module'] = 'navigate_favorites';
        query_array['action'] = 'sort';
        query_array[''] = $(this).sortable('serialize');
        Navigate.processAjax(wid, query_array);
      }
    });
  });
  
  // Bind delete links
  $(".navigate-favorites-delete").each(function () {
    if ($(this).hasClass('favorites-delete-processed')) {
      return;
    }
    $(this).addClass('favorites-delete-processed');
    $(this).click(function() {
      var callback = 'navigate_favorites_msg';
      var wid = $(this).parents('.navigate-widget').children('.wid').val();
      $('.navigate-favorites-list-' + wid).animate({'opacity': 0.4});
      var query_array = new Array();
      query_array['module'] = 'navigate_favorites';
      query_array['action'] = 'remove';
      query_array['fav_id'] = $(this).children('.navigate-favorites-id').val();
      Navigate.processAjax(wid, query_array, callback);
    });
  });
  
  
  // Show delete links when hovering
  $(".navigate-favorites-link-outer").each(function () {
    if ($(this).hasClass('navigate-favorites-link-outer-processed')) {
      return;
    }
    $(this).addClass('navigate-favorites-link-outer-processed');
    $(this).hover(
      function() {
        $(this).children('.navigate-favorites-delete').show();
      },
      function() {
        $(this).children('.navigate-favorites-delete').hide();
      }
    );
  });
  
  $('.navigate-favorites-export').focus(function () { $(this).select();});
  $('.navigate-favorites-import').focus(function () { $(this).select();});
}

/**
 * Process adding of a favorite
 */
function navigate_favorites_add(wid) {
  var callback = 'navigate_favorites_msg';
  
  $('.navigate-favorites-list-' + wid).animate({'opacity': 0.4});
  
  var query_array = new Array();
  query_array['module'] = 'navigate_favorites';
  query_array['action'] = 'add';
  query_array['name'] = $('#navigate-favorite-name_' + wid).val();
  Navigate.processAjax(wid, query_array, callback);
}


/**
 * Callback for positive result from ajax query
 */
function navigate_favorites_msg(msg, wid) {
  $('.navigate-favorites-list-' + wid).html(msg);
  $('.navigate-favorites-list-' + wid).animate({'opacity': 1}, function() {
    Drupal.attachBehaviors('.navigate-favorites-list-' + wid);
    $('.navigate-favorites-delete').hide();
  });
}

/**
 * Create export content and populate textarea
 */
function navigate_favorites_export(wid) {
  var callback = 'navigate_favorites_export_msg';
  
  $('.navigate-favorite-settings-' + wid).animate({'opacity': 0.4});
  
  var query_array = new Array();
  query_array['module'] = 'navigate_favorites';
  query_array['action'] = 'export';
  query_array['content'] = $('#navigate-favorites-export_' + wid).val();
  Navigate.processAjax(wid, query_array, callback);
}

/**
 * Callback for positive result from ajax query
 */
function navigate_favorites_export_msg(msg, wid) {
  $('#navigate-favorites-export_' + wid).val(msg);
  $('.navigate-favorite-settings-' + wid).animate({'opacity': 1.0});
}

/**
 * Create export content and populate textarea
 */
function navigate_favorites_import(wid) {
  if ($('#navigate-favorites-import_' + wid).val() == '') {
    if (!confirm("You are about to import nothing, which will clear your favorites list. Are you sure you want to do this?")) {
      return;
    }
  }
  var callback = 'navigate_favorites_import_msg';
  
  $('.navigate-favorite-settings-' + wid).animate({'opacity': 0.4});
  $('.navigate-favorites-list-' + wid).animate({'opacity': 0.4});
  
  var query_array = new Array();
  query_array['module'] = 'navigate_favorites';
  query_array['action'] = 'import';
  query_array['content'] = $('#navigate-favorites-import_' + wid).val();
  Navigate.processAjax(wid, query_array, callback);
}

/**
 * Callback for positive result from ajax query
 */
function navigate_favorites_import_msg(msg, wid) {
  $('#navigate-favorites-import_' + wid).val('');
  $('.navigate-favorite-settings-' + wid).animate({'opacity': 1.0});
  
  $('.navigate-favorites-list-' + wid).html(msg);
  $('.navigate-favorites-list-' + wid).animate({'opacity': 1}, function() {
    Drupal.attachBehaviors('.navigate-favorites-list-' + wid);
    $('.navigate-favorites-delete').hide();
  });
}