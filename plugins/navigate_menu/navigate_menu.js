
;(function($, $$) {   // Reference jQuery with $ and Navigate with $$

  $$.pluginCreate({

    // Plugin Settings    
    settings: {
      plugin:         'navigate_menu',
      pluginName:     'Menu',
      pluginVersion:  '2.0-beta1'
    },

    // Plugin Events
    events: {

      // Navigate Behaviors

            // NOTE: initialize will only run one time. There is no need to detect if this behavior has already ran, Navigate determines this automatically.
            //       This behavior is when the page is new and Navigate loads for the first time and you need to bind existing widgets. Any new content should
            //       attach behaviors dynamically after the successful ajax call. See the loadMenu method below for usage example.
            //
            // initialize: function(data) {
            //   $$.execute({ event: 'bindTreeview', scope: 'navigate_menu' }, data);
            // },
      
            // NOTE: bindWidget is called when a widget is created, duplicated or loaded. On startup, Navigate invokes initialize which in turn invokes bindWidget.
            //       So there is no need to have a initialize behavior for navigate_menu as this would simply duplicate an attachBehaviors call on bindTreeview.
            bindWidget: function(data) {
              $$.execute({ event: 'bindTreeview', scope: 'navigate_menu', context: $$.getWidget('navigate_menu') }, data);
            },

      // Plugin behaviors (although, any plugin could call this behavior if needed)

            bindTreeview: function(data) {
              $('.navigate-menu-list', data.context).each(function () {
                if ($(this).hasClass('navigate-menu-list-processed')) {
                  return;
                }
                $(this).addClass('navigate-menu-list-processed');
                var wid = $$.getWid(this);
                $(this).treeview({
                  animated: "fast",
                  collapsed: true,
                  persist: "cookie",
                  cookieId: "navigationtree_" + wid,
                  toggle: function(){
                    $$.slideWidget(); 
                  }
                });
              });
            }
    },

    // Plugin Methods/Callbacks
    loadMenu: function(wid) {
      // Change the widget title to match the selected menu item
      var widget = $$.getWidget(wid);
      var content = $('.navigate-widget-content', widget)
      var settings = $('.navigate-widget-settings', widget)
      var select = settings.find('select :selected');
      var title = select.text();
      var mid = select.val();
      var menu = mid.split(':');
      if (menu[1] == 0) {
        title = menu[0].replace(/^(.)|-(.)/g, function ($1) {
                return $1.toUpperCase();
        }).replace(/-/, " ");
      }
      else {
        title = title.replace(/[-<>]/g, '').replace(/^\s+|\s+$/g,"");
      }
      // Load the menu
      $$.ajax({
        data: {
          'mid':    $(select).val(),
          'op':     'output',
          'plugin': 'navigate_menu',
          'wid':    wid
        },
        success: function(json) {
          content.html(json.content);
          $$.execute({ event: 'bindTreeview', scope: 'navigate_menu', context: content });
          $$.changeTitle(wid, title);
          $$.toggleWidgetSettings(wid);
        }
      });
    }
  });

})(Navigate.jQuery, Navigate);
