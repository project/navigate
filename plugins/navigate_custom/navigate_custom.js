;(function($) {
  Navigate.pluginCreate({
    
    // Plugin Settings    
    settings: {
      plugin:         'navigate_custom',
      pluginName:     'Custom',
      pluginVersion:  '2.0-beta1'
    },
    
    // Save the content and reload it
    save: function(wid) {
      $('.navigate-custom-output-' + wid).animate({'opacity': 0.4});
      Navigate.ajax({
        data: {
          'wid':      wid,
          'module':   'navigate_custom',
          'action':   'save',
          'content':  $('#navigate-content-input_' + wid).val()
        },
        success: function(wid, msg) {
          $('.navigate-custom-output-' + wid).html(msg);
          $('.navigate-custom-output-' + wid).animate({'opacity': 1});
        }
      });
    }
  })
})(jQuery);
