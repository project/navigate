
;(function($, $$) {

  $$.pluginCreate({
    
    // Plugin Settings    
    settings: {
      plugin:         'navigate_search',
      pluginName:     'Search',
      pluginVersion:  '2.0-beta1'
    },
    
    // Plugin behaviors
    events: {
      
      initialize: function(options) {
        // Highlight all content types when clicked
        $('.content-type-search-all:not(.content-type-search-all-processed)').each(function () {
          $(this).click(function () {
            //alert($(this).parents('.navigate-widget-settings').html());
            var $settings = $(this).parents('.navigate-widget-settings').find('.navigate-search-content-types');
            $settings.find('.navigate-button').addClass('navigate-button-on');
            var wid = $(this).parents('.navigate-widget-outer').children('.wid').val();
            var query_array = new Array();
            query_array['module'] = 'navigate_search';
            query_array['action'] = 'search_content_type_all';
            Navigate.processAjax(wid, query_array);
          });
        });

        // De-select all content types when clicked
        $('.content-type-search-none:not(.content-type-search-none-processed)').each(function () {
          $(this).click(function () {
            //alert($(this).parents('.navigate-widget-settings').html());
            var $settings = $(this).parents('.navigate-widget-settings').find('.navigate-search-content-types');
            $settings.find('.navigate-button').removeClass('navigate-button-on');
            //var wid = Navigate.getWid(this);
            var wid = $(this).parents('.navigate-widget-outer').children('.wid').val();
            var query_array = new Array();
            query_array['module'] = 'navigate_search';
            query_array['action'] = 'search_content_type_all';
            query_array['none'] = 1;
            Navigate.processAjax(wid, query_array);
          });
        });
        
        // Bind keyboard shortcut enable / disable
        $('.navigate-key-button:not(.navigate-key-button-search-processed)').each(function () {
          $(this).addClass('navigate-key-button-search-processed');

          $(this).click(function () {
            if ($(this).hasClass('key-disabled')) {
              $(document).bind('keydown', {combi:'ctrl+shift+s'});
            } else {
              $(document).bind('keydown', {combi:'ctrl+shift+s'});
            }
          });
        });
      },

      bindKeys: function(options) {
        if ($('.navigate-key-button').hasClass('key-disabled')) {
          $(options.context).unbind('keydown', {combi:'ctrl+shift+s'});
        }
        else {
          $(options.context).bind('keydown', {combi:'ctrl+shift+s'}, function() {
            $$.show();$('.navigate-search-phrase').focus();
          });
        }
      }
    },
    
    // Plugin Methods/Callbacks
    
    // Grab search variables and send to be processed
    process: function(wid) {
      $('.navigate-search-results-' + wid).slideUp('fast', function() {
        $$.ajax({
          data: {
            'wid':      wid,
            'plugin':   'navigate_search',
            'action':   'search',
            'phrase':   $('#navigate-search-phrase_' + wid).val()
          },
          success: function(json) {
            $('.navigate-search-results-' + wid).html(json.data);
            $('.navigate-search-results-' + wid).slideDown('fast', function() {
              $('.navigate-search-results-' + wid).removeAttr('style'); // slideUp and slideDown add some styles we need to get rid of before saving
              Drupal.attachBehaviors();
            });
          }
        });
      });
    }
  });
})(Navigate.jQuery, Navigate);
