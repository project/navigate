
/**
 * Save the content and reload it
 */
function navigate_custom_save(wid) {
  $('.navigate-block-output-' + wid).animate({'opacity': 0.4});
  Navigate.ajax({
    data: {
      'wid': wid,
      'module':   'navigate_block',
      'action':   'save',
      'content':  $('#navigate-content-input_' + wid).val()
    },
    success: function(){
      $('.navigate-block-output-' + wid).html(msg);
      $('.navigate-block-output-' + wid).animate({'opacity': 1});      
    };
}
