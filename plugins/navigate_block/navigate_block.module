<?php

/**
 * @file
 * Produces the Navigate Custom code widget
 */

/**
 * Implementation of hook_init().
 */
function navigate_block_init() {
  if (user_access('navigate_block use')) {
    drupal_add_js(drupal_get_path('module', 'navigate_block') .'/navigate_block.js', 'module', 'navigate');
    drupal_add_css(drupal_get_path('module', 'navigate_block') .'/navigate_block.css');
  }
}


/**
 * Implementation of hook_navigation_widgets().
 */
function navigate_block_navigate_plugin($op, $settings= array()) {
  switch ($op) {
    case 'output':
      switch ($settings['plugin']) {
        case 'custom':
          return navigate_block_widget($settings['wid']);
          break;
      }
      break;

    case 'widget':
      return array(
        'widget' => array(        
          'navigate_block' => array(
            'callback' => NULL,
            'content'  => 'Block',
            'name'     => 'Block',
            'single'   => FALSE,
          ),
        )
      );
      break;

    case 'delete':
      break;
  }
}


/**
 * Generate search widget
 */
function navigate_block_widget($wid) {
  $settings = navigate_widget_settings_get($wid);

  $inputs['content']  = navigate_textarea(array(
    'name' => 'content',
    'class' => 'navigate-content-input',
    'filters' => 'true',
    'callback' => 'navigate_block_save',
    'help' => 'Type your custom code in here',
    'wid' => $wid,
  ));

  $output = theme('navigate_block_widget', $inputs, $wid);
  return $output;
}


/**
 * Theme search widget
 */
function theme_navigate_block_widget($inputs, $wid) {
  $content['widget'] = '
    <div class="navigate-custom-output navigate-custom-output-'. $wid .'">'. navigate_block_output($wid) .'</div>';
  $content['title'] = t('Custom');
  $content['settings'] = '
    <div class="navigate-custom-inputs">
      <div class="navigate-custom-input-outer">
        '. $inputs['content'] .'
      </div>
    </div>';

  return $content;
}


/**
 * Implementation of hook_perm().
 */
function navigate_block_perm() {
  return array("navigate_block use");
}


/**
 * Implementation of hook_navigate_widget_process().
 */
function navigate_block_navigate_widget_process($wid, $action) {
  switch ($action) {
    case 'save':
      echo navigate_block_save();
      break;
  }
}


/**
 * Save the content of a custom widget
 */
function navigate_block_save() {
  $output = navigate_block_output($_POST['wid']);
  return $output;
}


/**
 * Output the saved content
 */
function navigate_block_output($wid) {
  $settings = navigate_widget_settings_get($wid);
  $settings['content_format'] = isset($settings['content_format']) ? $settings['content_format'] : '';
  $settings['content'] = isset($settings['content']) ? $settings['content'] : t('Click this widget\'s settings button to change this content.');
  $output = check_markup($settings['content'], $settings['content_format']);
  return $output;
}


/**
 * Implementation of hook_navigate_help_page().
 */
function navigate_block_navigate_help_page() {
  $help['content'] = t('<p>The Custom widget allows you to add arbitrary code to your Navigate bar, based on what input filters you have available to you. This includes PHP code, for example, if you are allowed to use that filter. Click the settings icon in the upper right hand corner of the widget to display a textbox where you can add your code.</p>');
  $help['title'] = 'Custom';
  $help['access'] = user_access('navigate_block use');
  return $help;
}


/**
 * Implementation of hook_theme().
 */
function navigate_block_theme() {
  return array(
    'navigate_block_widget' => array(
      'arguments' => array('inputs' => NULL, 'wid' => NULL),
    ),
  );
}