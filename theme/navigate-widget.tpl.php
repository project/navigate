<?php
 /**
  * This template is used to output a single widget in Navigate. 
  *
  * Variables available:
  * - $widget: The widget object
  *   - content:    The widget output content
  *   - customize:  The customize class
  *   - data:       The hidden input containing vital Navigate data. This field is required and must be the first item place inside the widget DIV
  *   - plugin:     The plugin this widget belongs to
  *   - settings:   The widget settings (see: navigate-widget-settings.tpl.php)
  *   - title:      The title of the widget
  *   - wid:        The widget ID
  */  
?>
<div id="navigate-widget-<?php print $widget->wid; ?>" class="navigate-widget">
  <?php print $widget->data; ?>
  <?php print !empty($widget->settings) ? '<div class="navigate-widget-settings-button"></div>' : ''; ?>
  <div class="navigate-widget-title<?php print $widget->customize; ?>"><?php print $widget->title; ?></div>
  <div class="navigate-widget-slider">
    <?php if (!empty($widget->settings)) { ?>
    <div class="navigate-widget-settings">
      <?php print $widget->settings; ?>
    </div>
    <?php } ?>
    <div class="navigate-widget-content">
      <?php print $widget->content; ?>
    </div>
  </div>
</div>