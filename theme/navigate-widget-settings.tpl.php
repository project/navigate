<?php
 /**
  * This template is used to render each widget setting as a tab.
  *
  * Variables available:
  * - $settings: The settings array of the widget
  */  
  
  $output = '';
  if (!empty($settings)) {
    $content = '';
    $tabs = '<ul class="navigate-widget-settings-tabs">';
    foreach ($settings as $tab) {
      $tabs .= '<li><a href="#navigate-widget-settings-' . strToLower($tab['title']) . '" title="' . ($tab['description'] ? $tab['description'] : $tab['title']) . '">' . $tab['title'] . '</a></li>';
      $content .= '<div id="navigate-widget-settings-' . strToLower($tab['title']) . '">' . $tab['content'] . '</div>';
    }
    $output = $tabs . '</ul>' . $content;
  }
  print $output;