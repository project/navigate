<?php
 /**
  * This template is used to output Navigate. 
  *
  * Variables available:
  * - $settings: The settings dialog array
  */
?>
<div id="navigate-settings">
<?php
  $content = '';
  $tabs = '  <ul class="navigate-settings-tabs">';
  foreach ($settings as $tab) {
    $tabs .= '
    <li>
      <a href="#navigate-settings-' . strToLower(str_replace(' ', '-', $tab['title'])) . '" title="' . ($tab['description'] ? $tab['description'] : $tab['title']) . '">
        ' . ($tab['data'] ? $tab['data'] : '') . '
        ' . $tab['title'] . '
      </a>
    </li>';
    $content .= '  <div id="navigate-settings-' . strToLower(str_replace(' ', '-', $tab['title'])) . '">';
    if (!empty($tab['section'])) {
      foreach ($tab['section'] as $section) {
        $content .= '    <h2 class="section-header">' . $section['header'] . '</h2>';
        $content .= '    <div class="section-content">' . ($section['content'] ? $section['content'] : $section['empty']) . '</div>';
      }
    }
    $content .=   '  </div>';
  }
  print $tabs;
  print '  </ul>';
  print $content;
?>
</div>