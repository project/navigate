<?php

function theme_navigate_admin_settings($form) {
  drupal_add_tabledrag('navigate-admin-settings-permission-rules', 'order', 'sibling', 'weight');
  $header = array(
    array('data' => t('Enabled'), 'header' => TRUE, 'style' => 'padding-left: 40px;'),
    array('data' => t('Type'), 'header' => TRUE),
    array('data' => t('Roles/Users'), 'header' => TRUE),
    array('data' => t('Operations'), 'header' => TRUE, 'colspan' => 2, 'style' => 'width: auto;'),
    array('data' => t('Weight')),
  );
  $rows = array();
  foreach (element_children($form['permission']['rules']) as $rid) {
    $rule =& $form['permission']['rules'][$rid];
    
    $roles = array();
    $users = array();
    $tokens = explode(',', $rule['ids']['#value']);
    foreach ($tokens as $token) {
      $token = explode(':', $token);
      $type = $token[0];
      $id = $token[1];
      switch ($type) {
        case 'rid':
          $result = db_query("SELECT name FROM {role} WHERE rid = '%d'", $id);
          while ($role = db_fetch_object($result)) {
            $roles[] = $role->name;
          }
          break;
        case 'uid':
          $result = db_query("SELECT name FROM {users} WHERE uid = '%d'", $id);
          while ($user = db_fetch_object($result)) {
            $users[] = $user->name;
          }
          break;
      }
    }
    
    $role_text = empty($roles) ? '' : '<div>' . format_plural(count($roles), '1 role', '@count roles') . ' <a href="#" class="ids">show</a><ul class="ids"><li>' . implode('</li><li>', $roles) . '</li></ul></div>';
    $user_text = empty($users) ? '' : '<div>' . format_plural(count($users), '1 user', '@count users') . ' <a href="#" class="ids">show</a><ul class="ids"><li>' . implode('</li><li>', $users) . '</li></ul></div>';
    
    $rule['ids']['#value'] = t('!roles!users', array(
      '!roles' => $role_text,
      '!users' => $user_text,
    ));
    
    $row = array();
    $row[] = array('data' => drupal_render($rule['status']), 'style' => 'width: 45%; vertical-align: top;');
    $row[] = array('data' => ucwords(drupal_render($rule['type'])), 'style' => 'vertical-align: top; width: 25%;');
    $row[] = array('data' => '<em>' . drupal_render($rule['ids']) . '</em>', 'style' => 'vertical-align: top; width: 25%;');
    $row[] = array('data' => t('<a href="@edit">Edit</a>', array('@edit' => url('admin/settings/navigate/rule/' . $rid . '/edit'))), 'style' => 'width: 1px; vertical-align: top;');
    $row[] = array('data' => t('<a href="@delete">Delete</a>', array('@delete' => url('admin/settings/navigate/rule/' . $rid . '/delete'))), 'style' => 'width: 1px; vertical-align: top;');
    $row[] = array('data' => drupal_render($rule['weight']));
    $rows[] = array('data' => $row, 'class' => 'draggable');
  }
  if (empty($rows)) {
    $rows[] = array(
      array('data' => '<em>'. t('There are currently no permission rules.') .'</em>', 'colspan' => 4),
      array('data' => ''),
      array('data' => ''),
      array('data' => ''),
      array('data' => ''),
      array('data' => '<span class="weight"></span>'),
    );
  }
  $form['permission']['table'] = array(
    '#value' => theme('table', $header, $rows, array('id' => 'navigate-admin-settings-permission-rules')),
  );
  $output = drupal_render($form);
  return $output;
}

function theme_navigate_admin_settings_permission_rule($form) {
  $output = '';
  $op = $form['#op'];
  switch ($op) {
    // Delete
    case 'delete':
      break;
    case 'error':
      break;
    // View
    case 'view':
      break;
    // Add / Edit
    default:
      $header = array();
      $plugins = array();
      $permissions = array();
      $header[] = array('data' => '', 'header' => TRUE, 'style' => 'text-align: center; width: auto; padding: 4px 10px;');
      foreach($form['permissions']['#value'] as $plugin => $data) {
        $plugins[] = $plugin;
        $header[] = array('data' => t($plugin), 'header' => TRUE, 'style' => 'text-align: center; width: 10px; padding: 4px 10px;');
        foreach($data as $permission) {
          $permissions[] = $permission;
        }
      }  
      $permissions = array_unique($permissions);
      sort($permissions);
      foreach($permissions as $key => $permission) {
        foreach($plugins as $plugin) {
          $permissions[$permission][$plugin] = in_array($permission, $form['permissions']['#value'][$plugin]) ? TRUE : FALSE;      
        }
        unset($permissions[$key]);
      }
      $rows = array();
      foreach ($permissions as $permission => $plugins) {
        $row = array();
        $row[] = array('data' => t($permission), 'style' => 'text-align: right; width: auto; padding: 4px 10px;');
        foreach($plugins as $plugin => $check) {
          if ($check) {
            $row[] = array('data' => drupal_render($form['permissions'][$plugin . ':' . str_replace(' ', '_', $permission)]), 'style' => 'text-align: center; width: 10px; padding: 4px 10px;');
          }
          else {
            $row[] = array('data' => '');
          }
        }
        $rows[] = array('data' => $row);
      }
      $form['permissions']['#value'] = theme('table', $header, $rows, array('style' => 'width: auto;'));
      break;
  }
  return drupal_render($form);
}

function theme_navigate_fieldset($element) {
  $class = array('form-fieldset');
  if (!empty($element['#collapsible'])) {
    drupal_add_js('misc/collapse.js');
    $class[] = 'collapsible';
    if (!empty($element['#collapsed'])) {
      $class[] = 'collapsed';
    }
  }
  _form_set_class($element, $class);
  return '<fieldset'. drupal_attributes($element['#attributes']) .'>'. ($element['#title'] ? '<legend>'. $element['#title'] .'</legend>' : '') . (isset($element['#description']) && $element['#description'] ? '<div class="description">'. $element['#description'] .'</div>' : '') . (!empty($element['#children']) ? $element['#children'] : '') . (isset($element['#value']) ? $element['#value'] : '') ."</fieldset>\n";
}

/**
 * Themes the outside of all widgets
 */
function theme_navigate_all_widgets($output) {
  $output =  '<div class="navigate-widget-list'. (user_access('navigate customize') ? ' customize' : '') .'">'. $output .'</div>';
  return $output;
}

/**
 * Theme a link
 */
function theme_navigate_link($menu_item) {
  static $i;
  if ($i == '') {
    $i = 1;
  }
  $char_count = 26;
  if (drupal_strlen($menu_item['title']) > $char_count) {
    $title = drupal_substr($menu_item['title'], 0, $char_count-3) .'...';
  }
  else {
    $title = $menu_item['title'];
  }

  $menu_item['description'] = isset($menu_item['description']) ? ('<div class="navigate-link-description">'. $menu_item['description'] .'</div>') : '';

  $help_content = '
    <div class="navigate-link-help">
      <div class="navigate-link-title">'. $menu_item['title'] .'</div>
      '. $menu_item['description'] .'
      <div class="navigate-link-link">'. url($menu_item['path']) .'</div>
    </div>
  ';
  $query = '';
  if (strpos($menu_item['path'], '?') !== FALSE) {
    $path_array = explode('?', $menu_item['path']);
    $menu_item['path'] = $path_array[0];
    $query = $path_array[1];
  }
  $id = 'navigate_link_'. $menu_item['wid'] .'_'. $i;
  $output = '<div id="'. $id .'" title="" class="navigate-tooltip navigate-link">'. l($title, $menu_item['path'], array('query' => $query)) .'</div>
    '. navigate_tooltip($id, $help_content);
  $i++;
  return $output;
}

/**
 * Theme a widget list item
 */
function theme_navigate_settings_add_widget($data) {
  $data['single'] = $data['single'] ? 1 : 0;
  $output = '<li class="navigate-plugin">
               '. $data['name'] .'
               <input type="hidden" class="navigate-data" value="' . htmlspecialchars(drupal_to_js($data)) . '"></input>
             </li>';
  return $output;
}
