<?php
 /**
  * This template is used to output Navigate. 
  *
  * Variables available:
  * - $navigate: The Navigate object
  *   - js:             The file(s) containing vital Navigate JavaScript. This field must be placed in this template or Navigate will break.
  *   - settings:       The rendered settings dialog (see: navigate-settings.tpl.php)
  *   - widgets:        The rendered output of current user widgets (see: navigate-widget.tpl.php)
  */  
?>

<div id="navigate-widget-remove">
  <div><?php print t('Drop Here'); ?><br /><?php print t('To Remove Widget'); ?></div>
</div>
<div id="navigate">
  <div id="navigate-top">
    <div id="navigate-switch">Navigate</div>
  <?php if (navigate_user_access('configure settings')) { ?>
    <div id="navigate-settings-button">Settings</div>
  <?php } ?>
    <div id="navigate-queue"></div>
  </div>
  <div id="navigate-content">
    <?php print $navigate->widgets; ?>
  </div>
  <div id="navigate-bottom">
    <div id="navigate-key-button" class="navigate-key-button"><?php print t('KEY'); ?></div>
    <div id="navigate-help-button" class="navigate-help-button"><?php print t('HELP'); ?><input type="hidden" class="value" value="<?php print url('admin/help/navigate'); ?>" /></div>
  </div>
  <div id="navigate-overlay"></div>
  <?php print $navigate->js; ?>
</div>
<?php print $navigate->settings; ?>
