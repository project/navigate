<?php

/**
 * @file
 * Drupal functions for defining Navigate as a module
 */


/**
 * Initialize Navigate global variable and reference it to the current session
 */
// unset($_SESSION['navigate']);
if (!isset($_SESSION['navigate'])) {
  $_SESSION['navigate'] = array(
    'settings' => array(
      'rules' => array(),
    ),
    'users' => array(
      'default' => array(
        'settings' => array(
          'keys'        => TRUE,
          'visible'     => TRUE,
          'widgetIndex' => 0,
        ),
      ),
    ),
  );
}
$GLOBALS['_navigate'] =& $_SESSION['navigate'];

/**
* Menu callback
*/
function navigate_rule_load($rid) {
  if (!is_numeric($rid)) {
    return NULL;
  }
  return db_fetch_object(db_query('SELECT * FROM {navigate_permissions} WHERE rid = %d', $rid));
}

/**
 * Implementation of hook_menu().
 */
function navigate_menu() {
  return array(
    // Navigate Settings
    'admin/settings/navigate' => array(
      'title' => t('Navigate'),
      'file' => 'includes/admin.inc',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('navigate_admin_settings'),
      'access arguments' => array('navigate administer'),
      'description' => t('Global configuration of Navigate functionality.'),
      'type' => MENU_NORMAL_ITEM,
    ),
    // Navigate Permissions
    'admin/user/permissions/list' => array(
      'title' => t('Modules'),
      'weight' => -10,
      'type' => MENU_DEFAULT_LOCAL_TASK,
    ),
    'admin/user/permissions/navigate' => array(
      'title' => t('Navigate'),
      'file' => 'includes/permissions.inc',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('navigate_permission_settings'),
      'access arguments' => array('navigate administer'),
      'description' => t('Grant configuration of Navigate functionality.'),
      'weight' => -9,
      'type' => MENU_LOCAL_TASK,
    ),
    'admin/user/permissions/navigate/rule' => array(
      // 'title' => t('Add Permission Rule'),
      'file' => 'includes/permissions.inc',
      'access arguments' => array('navigate administer'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('navigate_permission_rule', 'add'),
      'type' => MENU_LOCAL_TASK,
    ),
    'admin/user/permissions/navigate/rule/%navigate_rule' => array(
      // 'title' => t('View Permission Rule'),
      'file' => 'includes/permissions.inc',
      'access arguments' => array('navigate administer'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('navigate_permission_rule', 'view', 5),
      'type' => MENU_LOCAL_TASK,
    ),
    'admin/user/permissions/navigate/rule/%navigate_rule/delete' => array(
      'access arguments' => array('navigate administer'),
      // 'title' => t('Delete Permission Rule'),
      'file' => 'includes/permissions.inc',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('navigate_permission_rule', 'delete', 5),
      'type' => MENU_LOCAL_TASK,
    ),
    'admin/user/permissions/navigate/rule/%navigate_rule/edit' => array(
      'access arguments' => array('navigate administer'),
      // 'title' => t('Edit Permission Rule'),
      'file' => 'includes/permissions.inc',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('navigate_permission_rule', 'edit', 5),
      'type' => MENU_LOCAL_TASK,
    ),
    // Ajax Callbacks
    'ajax/navigate' => array(
      'access arguments' => array('navigate view'),
      'file' => 'includes/core.inc',
      'page callback' => 'hook_navigate_ajax',
      'type' => MENU_CALLBACK,
    ),
    'ajax/navigate/autocomplete' => array(
      'access arguments' => array('navigate administer'),
      'file' => 'includes/core.inc',
      'page callback' => 'navigate_autocomplete',
      'type' => MENU_CALLBACK,
    ),
  );
}

/**
 * Implementation of hook_init().
 */
function navigate_init() {
  global $_navigate;
  // Include core functions
  include_once dirname(__FILE__) . '/includes/core.inc';
  // Intialize settings
  $settings = navigate_settings_get(FALSE, NULL, TRUE);
  // Match path if necessary
  if ($settings['pages']) {
    if ($settings['visibility'] < 2) {
      $path = drupal_get_path_alias($_GET['q']);
      // Compare with the internal and path alias (if any).
      $_navigate['page_match'] = drupal_match_path($path, $settings['pages']);
      if ($path != $_GET['q']) {
        $_navigate['page_match'] = $_navigate['page_match'] || drupal_match_path($_GET['q'], $settings['pages']);
      }
      // When $settings['visiblity'] has a value of 0, Navigate is displayed on
      // all pages except those listed in $settings['pages']. When set to 1, it
      // is displayed only on those pages listed in $settings['pages'].
      $_navigate['page_match'] = !($settings['visibility'] xor $_navigate['page_match']);
    }
    else {
      $_navigate['page_match'] = drupal_eval($settings['pages']);
    }
  }
  else {
    $_navigate['page_match'] = TRUE;
  }
  if ($_navigate['page_match'] && navigate_user_access('view')) {
    // Initialize Navigate User
    navigate_user_load(FALSE, TRUE);
    // Load default steel CSS Navigate theme
    navigate_theme_load('steel');
    // Load JavaScript using Navigate wrapper
    navigate_add_js(array(
      'xregexp.js',
      'jquery.cookie.js',
      'jquery.hotkeys.js',
      'jquery.hoverintent.js',
      'jquery.mousewheel.js',
      'jquery.mwheelintent.js',
      'jquery.jscrollpane.js',
      'jquery.pngFix.js',
      'jquery.tooltip.js',
      'jquery.shadowanimation.js',
      'navigate.js',
    ));
    // JavaScript Settings
    $visible = (bool) navigate_variable_get('visible');
    $settings = array(
    'Navigate' => array(
      'modules' => array(
        'admin'                   => module_exists('admin'),
        'admin_menu'              => module_exists('admin_menu'),
        'javascript_aggregator'   => module_exists('javascript_aggregator'),
      ),
      'settings' => array(
        'administer'  => user_access('navigate administer'),
        'customize'   => user_access('navigate customize'),
        'destination' => '/' . $_GET['q'],
        'visible'     => (bool) $visible,
      ),
      'theme' => navigate_less_variables('theme.steel.css'),
    ));
    drupal_add_js($settings, 'setting');
  }
  if (user_access('navigate administer')) {
    drupal_add_js('misc/autocomplete.js');
  }
}

/**
 * Implementation of hook_theme_registry_alter().
 *
 * Make Navigate's page preprocess function run after everything else's,
 * so that Navigate can prepend it's initialization JS file.
 */
function navigate_theme_registry_alter(&$theme_registry) {
  if (isset($theme_registry['page'])) {
    // array_unshift($theme_registry['page']['theme paths'], drupal_get_path('module', 'navigate') . '/theme');
    if (count($theme_registry['page']['preprocess functions']) > 0) {
      if ($key = array_search('navigate_preprocess_page', $theme_registry['page']['preprocess functions'])) {
        unset($theme_registry['page']['preprocess functions'][$key]);
      }
    }
    $theme_registry['page']['preprocess functions'][] = 'navigate_preprocess_page';
  }
}

function navigate_preprocess_page(&$variables) {
  if (navigate_user_access('view')) {
    $variables['scripts'] = '
<!--  Initialize Navigate and push the latest jQuery and jQuery UI minified versions (from Google\'s CDN) into Navigate.
      NOTE: These versions will NOT upgrade or impact the existing jQuery and/or jQuery UI versions of this site.
            These versions are only used by Navigate and it\'s plugins. -->
      <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script> 
      <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js"></script> 
      <script type="text/javascript">
        <!--//--><![CDATA[//><!--
          var Navigate=Navigate||{};if(window.jQuery){Navigate.jQuery=jQuery.noConflict(true);}else{Navigate.jQuery = false;alert("Navigate cannot load the latest jQuery version from Google\'s CDN. Please check your internet connection.");}
        //--><!]]>
      </script>
<!-- End -->

' . $variables['scripts'];
    // $variables['styles'] = '<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" media="all" rel="stylesheet" type="text/css"></link>' . $variables['styles'];
  }
}

/**
 * Implementation of hook_help().
 */
function navigate_help($path, $arg) {
  $toc = '';
  switch ($path) {
    case 'admin/help#navigate':
      $toc .= '<li><a href="#widget_help_main">General</a></li>';
      $content = t(
        '<a name="widget_help_main"></a>
        <h3>What is Navigate?</h3>
        <p>Navigate is the bar of widgets you see to your left. Or, if Navigate is minimized, you will see the Navigate icon peeking out of the upper left corner of the screen. When you click this icon, you will see the full Navigate widget set. Depending on the modules installed, Navigate will automatically add a Search widget and a Favorites widget for new users.</p>
        <p>Navigate uses a cache to save each user\'s Navigate bar whenever it is changed. This means that it requires few system resources when navigating from page to page.</p>
        <h3>The basic tools</h3>
        <p>Additional widgets can be added to your bar by clicking the add / remove widgets button, which looks like a cog wheel in the upper right hand corner of Navigate. Each user has their own Navigate bar, so you can customize yours to fit your needs.</p>
        <ul>
          <li><strong>For help on how to use a widget</strong>, you can often hover over the part in question until a tooltip displays.</li>
          <li><strong>To hide / show the Navigate bar</strong>, click the steering wheel icon in the upper left corner of the Navigate bar, or if it is hidden, on the part of the icon that displays in the upper left hand corner of the screen.</li>
          <li><strong>To add or remove widgets</strong>, click the cog wheel icon in the upper right corner of the Navigate bar. A small (x) icon will appear next to your widget title, which you can click to remove the widget.</li>
          <li><strong>To re-order your widgets</strong>, drag them by their title to the desired location and then let go.</li>
          <li><strong>To change the title of a widget</strong>, double-click the title, enter your new text and press enter.</li>
          <li><strong>To access the advanced settings of a widget</strong>, if advanced settings are available, a small cog wheel icon will be visible in the upper right hand corner of the widget. Click this to expand the advanced settings pane.</li>
          <li><strong>To shrink the Navigate bar</strong>, double-click on the Navigate bar title (which says "navigate"). This will hide areas widgets have marked as superfluous. To get them to show again, double click the title once more.</li>
          <li>Press CTL+SHIFT+N to toggle Navigate visibility. Click the "KEY" button at the bottom of Navigate to disable keyboard shortcuts.</li>
        </ul>'
      );
      $hook = 'navigate_help_page';
      $array = array();
      $i = 0;
      foreach (module_implements($hook) as $module) {
        $function = $module .'_'. $hook;
        $array = $function();
        if ($array['access']) {
          $toc .= '<li><a href="#widget_help_'. $i .'">'. $array['title'] .' widget</a></li>';
          $content .= '<a name="widget_help_'. $i .'"></a><h2>'. $array['title'] .' widget<span class="navigate-toc-link"><a href="#toc">top</a></span></h2>'. $array['content'];
        }
      }
      $output = '<a name="toc"></a><ul>'. $toc .'</ul>'. $content;
      return $output;
  }
}

/**
 * Implementation of hook_footer().
 */
function navigate_footer() {
  global $_navigate, $user;
  $output = '';
  // Build navigate
  if ($_navigate['page_match'] && navigate_user_access('view')) {
    $obj = new StdClass;
    // Load default widget set(s) if user has no widgets
    $count = $_navigate[$user->uid]['widgets'];
    if ($count < 1) {
      $_navigate[$user->uid]['widgets'] = $_navigate['defaults']['widgets'];
      // navigate_save_user();
    }    
    // Settings dialog
    $obj->settings = theme('navigate_settings', hook_navigate_plugin());
    // Build widgets
    $obj->widgets = theme('navigate_all_widgets', navigate_build_widgets($user->uid));
    // JavaScript
    $obj->js = drupal_get_js('navigate');
    if (!empty($obj->js)) {
      if (module_exists('javascript_aggregator')) {
        $obj->js = _javascript_aggregator_minify($obj->js);
      }
    }
    // Theme Navigate
    $output = theme('navigate', $obj);
  }
  return $output;
}

/**
 * Load a widget set
 */
function navigate_defaults_load() {
  global $user;
  // Save current set
  if (navigate_variable_get('saved_set_for_loading', 0) == '') {
    $settings = array(
      'name' => 'saved_set_for_loading',
      'value' => serialize(navigate_export($user->uid)),
      'wid' => 0,
      );
    navigate_variable_set($settings);
  }

  // Replace out current
  if (isset($_POST['rid'])) {
    $export = variable_get('navigate_defaults_'. check_plain($_POST['rid']), '');
    navigate_import($export, $user->uid);
  }
  if (isset($_POST['username'])) {
    if ($uid = db_result(db_query("SELECT uid FROM {users} WHERE name = '%s'", $_POST['username']))) {
      $export = navigate_export($uid);
      navigate_import($export, $user->uid);
      navigate_variable_set(array('value' => check_plain($_POST['username']), 'name' => 'username_search'));
    }
  }
  echo navigate_build_defaults_admin();
  die();
}

/**
 * Return to saved widget set
 */
function navigate_defaults_switch_back() {
  global $user;
  $export = unserialize(navigate_variable_get('saved_set_for_loading', 0));
  navigate_import($export, $user->uid);
  navigate_variable_delete('saved_set_for_loading');
  echo navigate_build_defaults_admin();
  die();
}

/**
 * Change the default widget set for all users of a certain role
 */
function navigate_defaults_set_all() {
  global $user;
  $export = navigate_export($user->uid);
  if (isset($_POST['rid']) && $_POST['rid'] != 2) {
    if ($rid = db_result(db_query("SELECT rid FROM {role} WHERE rid = '%d'", $_POST['rid']))) {
      variable_set('navigate_defaults_'. $rid, navigate_export($user->uid));
      $result = db_query("SELECT uid FROM {users_roles} WHERE rid = '%d'", $_POST['rid']);
      while ($row = db_fetch_array($result)) {
        if (db_fetch_array(db_query("SELECT uid FROM {navigate_widgets} WHERE uid = '%d'", $row['uid']))) {
          navigate_import($export, $row['uid']);
        }
      }
    }
  }
  elseif (isset($_POST['rid']) && $_POST['rid'] == 2) {
    $result = db_query("SELECT DISTINCT(u.uid),  ur.rid FROM {users} u
      LEFT JOIN {users_roles} ur ON u.uid = ur.uid
      WHERE u.uid != 0
      AND ur.rid IS NULL
      GROUP BY u.uid");
    while ($row = db_fetch_array($result)) {
      if (db_fetch_array(db_query("SELECT uid FROM {navigate_widgets} WHERE uid = '%d'", $row['uid']))) {
        navigate_import($export, $row['uid']);
      }
    }
  }
  elseif (isset($_POST['username'])) {
    if ($uid = db_result(db_query("SELECT uid FROM {users} WHERE name = '%s'", $_POST['username']))) {
      navigate_import(navigate_export($user->uid), $uid);
      navigate_variable_set(array('value' => check_plain($_POST['username']), 'name' => 'username_search'));
    }
  }
  echo navigate_build_defaults_admin();
  die();
}

/**
 * Load a widget set associated with a user
 */
function navigate_user_load_set() {
  global $user;
  if (!user_access('navigate administer')) {
    return;
  }

}


/**
 * Reload the navigate widgets
 */
function navigate_widgets_reload() {
  global $user;
  $output = navigate_build_widgets($user->uid);
  echo $output;
  die();
}

/**
 * Save default widget set
 */
function navigate_save_defaults() {
  global $user;
  if (!user_access('navigate administer')) {
    return;
  }
  if (isset($_POST['rid'])) {
    if ($rid = db_result(db_query("SELECT rid FROM {role} WHERE rid = '%d'", $_POST['rid']))) {
      variable_set('navigate_defaults_'. $rid, navigate_export($user->uid));
    }
    if ($_POST['rid'] == 0) {
      variable_set('navigate_defaults_0', navigate_export($user->uid));
    }
  }
  if (isset($_POST['uid'])) {
    if ($uid = db_result(db_query("SELECT uid FROM {users} WHERE uid = '%d'", $_POST['uid']))) {
      navigate_import(navigate_export($user->uid), $uid);
    }
  }
  echo navigate_build_defaults_admin();
  die();
}

/**
 * Unset default widget set
 */
function navigate_defaults_unset() {
  global $user;
  if (!user_access('navigate administer')) {
    return;
  }
  variable_del('navigate_defaults_'. $_POST['rid']);
  echo navigate_build_defaults_admin();
  die();
}

/**
 * Deletes a widget
 */
function navigate_widget_delete($wid='', $uid = '') {
  global $user;
  if (!user_access('navigate administer') && $uid != $user->uid) {
    return FALSE;
  }
  if ($uid == '') {
    $uid = $user->uid;
  }
  $wid = ($wid == '') ? $_POST['wid'] : $wid;

  // Call widget delete operation
  $row = db_fetch_array(db_query("SELECT * FROM {navigate_widgets} WHERE wid = '%d' AND uid = '%d'", $wid, $uid));
  $function = $row['plugin'] .'_navigate_plugin';
  if (function_exists($function)) {
    $function('delete', array('wid' => $row['wid'], 'plugin' => $row['plugin']));
  }
  db_query("DELETE FROM {navigate_widgets} WHERE uid = '%d' AND wid = '%d'", $uid, $wid);
  db_query("DELETE FROM {navigate_user_settings} WHERE uid = '%d' AND wid = '%d'", $uid, $wid);
}


/**
 * Save the new sort order for the widgets
 */
function navigate_widget_sort() {
  global $user;
  foreach ($_POST['navigate-widget'] as $key => $wid) {
    db_query("UPDATE {navigate_widgets} SET weight = '%d' WHERE uid = '%d' AND wid = '%d'", $key, $user->uid, $wid);
  }
}


/**
 * Add a new widget
 */
function navigate_widget_add($plugin=NULL, $output=TRUE) {
  global $user;

  if (!$plugin) {
    $plugin = $_POST['plugin'];
  }
  if (!module_exists($plugin)) {
    return FALSE;
  }
  // Get next weight
  $weight = db_result(db_query_range("SELECT weight FROM {navigate_widgets} WHERE uid = '%d' ORDER BY weight DESC", $user->uid, 0, 1));
  $weight++;

  db_query("INSERT INTO {navigate_widgets} (uid, plugin, weight) VALUES (%d, '%s', %d)", $user->uid, $plugin, $weight);
  $wid = db_last_insert_id('navigate_widgets', 'wid');

  if ($output) {
    drupal_json(array(
      'wid'     => $wid,
      'content' => navigate_output_widget(db_fetch_array(db_query("SELECT * FROM {navigate_widgets} WHERE wid = '%d'", $wid)), 1),
    ));
  }
}

function navigate_build_widgets($uid) {
  global $_navigate;
  $output = '';
  $widgets = $_navigate['users'][$uid]['widgets'];
  uasort($widgets, 'navigate_sort_weights');
  foreach ($widgets as $widget) {
    if (module_exists($widget['plugin'])) {
      $output .= navigate_output_widget($widget);
    }
  }
  return $output;
}

function navigate_build_defaults_admin() {

  // Check if a widget set is loaded
  $alert = '';
  if (navigate_variable_get('saved_set_for_loading', 0) != '') {
    $alert = '<a href="'. url('navigate/unload') .'" class="navigate-default-switch-back">Switch back to your auto-saved set</a>';
  }

  // Run through roles
  $output = '';
  $result = db_query("SELECT * FROM {role} WHERE rid <> 1");
  while ($row = db_fetch_array($result)) {
    $default = variable_get('navigate_defaults_'. $row['rid'], '');
    $load = '<span class="default-disabled navigate-default-user-load">'. t('Load') .'<input type="hidden" class="rid" value="'. $row['rid'] .'" /></span>';
    $unset = '<span class="default-disabled navigate-default-user-load">'. t('Unset') .'</span>';
    if ($default != '') {
      $load = '<a class="navigate-default-load" href="javascript:;" rel="'. $row['rid'] .'">'. t('Load') .'</a>';
      $unset = '<a class="navigate-default-unset" href="javascript:;" rel="'. $row['rid'] .'">'. t('Unset') .'</a>';
    }
    $output .= '
      <tr>
        <td class="navigates-default-table-label">'. $row['name'] .'</td>
        <td><a class="navigate-default-set" href="javascript:;" rel="'. $row['rid'] .'">'. t('Set') .'</a></td>
        <td>'. $load .'</td>
        <td>'. $unset .'</td>
        <td><a class="navigate-default-set-all" href="javascript:;" rel="'. $row['rid'] .'">'. t('Set all users') .'</a></td>
      </tr>';
  }

  $default = variable_get('navigate_defaults_0', '');
  $load = '<span class="default-disabled navigate-default-user-load">'. t('Load') .'</span>';
  $unset = '<span class="default-disabled navigate-default-user-load">'. t('Unset') .'</span>';
  if ($default != '') {
    $load = '<a class="navigate-default-load" href="javascript:;">'. t('Load') .'</a>';
    $unset = '<a class="navigate-default-unset" href="javascript:;" rel="0">'. t('Unset') .'</a>';
  }

  // User search row
  $username_value = navigate_variable_get('username_search');
  $username_class = '';
  $load = '<span class="default-disabled navigate-default-user-load">'. t('Load') .'</span>';
  $set_user = '<span class="default-disabled navigate-default-user-replace">'. t('Set user') .'</span>';
  if ($username_value == '') {
    $username_value = t('Search name / UID');
    $username_class = ' navigate-username-empty ';
  }
  else {
    $load = '<a class="navigate-default-load-user" href="javascript:;">'. t('Load') .'</a>';
    $set_user = '<a class="navigate-default-set-user" href="javascript:;">'. t('Set user') .'</a>';
  }

  // Import / export
  $inputs['export']  = navigate_textarea(array(
    'name' => 'set-export',
    'class' => 'navigate-favorites-export',
    'filters' => 'true',
    'submit' => 'Export widget set',
    'hide_filters' => 1,
    'callback' => 'setExport',
    'help' => 'Click the export button and it will load the content in this box.',
    'wid' => '',
  ));

  $inputs['import']  = navigate_textarea(array(
    'name' => 'set-import',
    'class' => 'navigate-favorites-import',
    'filters' => 'true',
    'submit' => 'Import widget set',
    'hide_filters' => 1,
    'callback' => 'setImport',
    'help' => 'Click the import button to import widget set. Note that this will replace your current set.',
    'wid' => '',
  ));

  $import_export = '
    <div class="navigate-import-export">
      '. $inputs['export'] . $inputs['import'] .'
    </div>';

  // Combine output
  $output = '
    <div class="navigate-admin-tools-defaults">
      <div class="navigate-admin-tools-heading">'. t('Defaults') .'</div>
      '. $alert .'
      <table class="navigate-defaults-table">'. $output .'
        <tr class="navigate-user-default-row">
          <td>
            <div class="navigate-user-search-outer">
              <div><input id="navigate-set-username" value="'. $username_value .'" class="form-text form-autocomplete navigate-text-input'. $username_class .'" type="text" maxlength="60"/><input type="hidden" disabled="disabled" value="'. url('navigate/user_autocomplete') .'" id="navigate-set-username-autocomplete" class="autocomplete"/></div>
            </div>
          </td>
          <td></td>
          <td class="navigate-default-set-user-load">'. $load .'</td>
          <td></td>
          <td class="navigate-default-set-user-set">'. $set_user .'</td>
        </tr>
      </table>
      <div class="navigate-admin-tools-heading">'. t('Widget set import / export') .'</div>
      '. $import_export .'
    </div>';
  return $output;
}


/**
 * Build default set of widgets
 */
function navigate_add_default_widgets() {
  global $user;

  $export = '';

  // Get default widget set for user role
  foreach ($user->roles as $rid => $role) {
    $export = variable_get('navigate_defaults_'. $rid, '');
  }

  // If no default role sets exist, then use 'unassigned roles' one
  if ($export == '') {
    $export = variable_get('navigate_defaults_0', '');
  }

  // Run import
  if ($export != '') {
    navigate_import($export, $user->uid);
    return;
  }

  // If there is default set, add a search and favorites widget
  if ($export != '') {
    navigate_import($export, $user->uid);
    return;
  }
  
  $plugins = array(
    'navigate_search',
    'navigate_favorites',
  );
  foreach ($plugins as $plugin) {
    if (module_exists($plugin)) {
      navigate_widget_add($plugin, FALSE);
    }
  }
}

/**
 * Builds a widget for output
 */
function navigate_output_widget($widget) {
  $output = '';
  $function = $widget['plugin'] .'_navigate_plugin';
  if (function_exists($function)) {
    $output = $function('display', $widget);
    $widget['content'] = isset($output['content']) ? $output['content'] : '';
    $widget['settings'] = isset($output['settings']) ? $output['settings'] : array();
    $obj = new StdClass;
    $obj->content   = $widget['content'];
    $obj->customize = (user_access('navigate customize') ? ' customize' : '');
    $obj->data      = '<input type="hidden" class="navigate-data" value=\'' . navigate_to_js(array('wid' => $widget['wid'], 'plugin' => $widget['plugin'])) . '\'></input>';
    $obj->plugin    = $widget['plugin'];
    $obj->settings  = theme(array('navigate_widget_settings_' . $widget['wid'], 'navigate_widget_settings'), $widget['settings']);
    $obj->title     = $widget['title'];
    $obj->wid       = $widget['wid'];
    $output = theme(array('navigate_widget_' . $widget['wid'], 'navigate_widget'), $obj);
  }
  return $output;
}

/**
 * Get settings specific to a widget
 */
function navigate_widget_settings_get($wid, $clear=FALSE) {
  // static $settings;
  // if ($clear) {
  //   unset($settings[$wid]);
  // }
  // if (isset($settings[$wid])) {
  //   return $settings[$wid];
  // }
  // $result = db_query("SELECT * FROM {navigate_user_settings} WHERE wid = '%d'", $wid);
  // while ($row = db_fetch_array($result)) {
  //   $settings[$wid][$row['name']] = $row['value'];
  // }
  // if (isset($settings[$wid])) {
  //   return $settings[$wid];
  // }
}


/**
 * Output a toggle button
 */
function navigate_button($button_settings) {

  $defaults = array(
    'on' => '1',
    'off' => '0',
    'help' => '',
  );
  foreach ($defaults as $key => $val) {
    if (!key_exists($key, $button_settings)) {
      $button_settings[$key] = $val;
    }
  }
  $settings = navigate_widget_settings_get($button_settings['wid']);

  // If the setting hasn't set before, set it to the default
  if (!isset($settings[$button_settings['name']]) && $button_settings['default'] != '') {
    $settings = array(
      'name' => $button_settings['name'],
      'value' => $button_settings['default'],
      'wid' => $button_settings['wid'],
    );
    navigate_variable_set($settings);
    $settings = navigate_widget_settings_get($button_settings['wid'], TRUE);
  }

  $on_class = '';
  if ($settings[$button_settings['name']] == $button_settings['on']) {
    $on_class = ' navigate-button-on ';
  }
  $group_class = '';
  $group_input = '';
  if (isset($button_settings['group'])) {
    $group_class = ' '. $button_settings['group'] .' ';
    $group_input = '<input type="hidden" class="group" value="'. $button_settings['group'] .'" />';
  }
  $required = '';
  if (isset($button_settings['required'])) {
    $required = 1;
  }
  $callback = '';
  if (isset($button_settings['callback'])) {
    $callback = $button_settings['callback'];
  }
  $output = '
    <div class="navigate-button-outer">
      <div id="'. $button_settings['class'] .'-'. $button_settings['wid'] .'" class="navigate-button '. $button_settings['class'] . $on_class . $group_class .' navigate-tooltip">'. $button_settings['content'] .'</div>
      <div class="navigate-absolute">
        <input type="hidden" class="widget_type" value="'. $button_settings['widget_type'] .'" />
        <input type="hidden" class="on" value="'. $button_settings['on'] .'" />
        <input type="hidden" class="off" value="'. $button_settings['off'] .'" />
        <input type="hidden" class="name" value="'. $button_settings['name'] .'" />
        <input type="hidden" class="required" value="'. $required .'" />
        <input type="hidden" class="callback" value="'. $callback .'" />
        '. navigate_tooltip($button_settings['class'] .'-'. $button_settings['wid'], $button_settings['help']) .'
        '. $group_input .'
      </div>
    </div>';
  return $output;
}


function navigate_tooltip($id, $content) {
  return '<div id="'. $id .'_tip_content" style="display:none">'. $content .'</div>';
}


/**
 * Output a text input
 */
function navigate_input($input_settings) {
  $settings = navigate_widget_settings_get($input_settings['wid']);
  if ($input_settings['select_all']) {
    $select_all_class = ' navigate-select-all ';
  }
  $clear_class = '';
  if (isset($input_settings['clear'])) {
    $clear_class = ' navigate-clear ';
  }
  $settings[$input_settings['name']] = isset($settings[$input_settings['name']]) ? ($settings[$input_settings['name']]) : '';
  $output = '
    <div class="navigate-text-input-outer">
      <div class="navigate-text-input-container"><input id="'. $input_settings['class'] .'_'. $input_settings['wid'] .'" class="navigate-text-input '. $input_settings['class'] . $select_all_class . $clear_class .' navigate-tooltip" value="'. $settings[$input_settings['name']] .'" name="'. $input_settings['class'] .'" /></div>
      <input type="hidden" class="navigate-data" value="' . htmlspecialchars(drupal_to_js($input_settings)) . '"></input>
      '. navigate_tooltip($input_settings['class'] .'_'. $input_settings['wid'], $input_settings['help']) .'
    </div>';
  return $output;
}


/**
 * Output a textarea input
 */
function navigate_textarea($input_settings) {
  $filters_output = '';
  $input_settings['select_all'] = isset($input_settings['select_all']) ? $input_settings['select_all'] : '';
  $settings = navigate_widget_settings_get($input_settings['wid']);

  $select_all_class = isset($input_settings['select_all']) ? ' navigate-select-all ' : '';

  $filters_output = '';
  if (!isset($input_settings['hide_filters'])) {
    $filters = filter_formats();
    foreach ($filters as $filter) {
      if (!isset($default)) {
        $default = $filter->format;
      }
      $filters_output .= navigate_button(array(
        'name' => $input_settings['name'] .'_format',
        'content' => $filter->name,
        'class' => 'navigate-filter-'. $filter->format,
        'group' => $input_settings['name'] .'_format',
        'default' => $default,
        'help' => '',
        'on' => $filter->format,
        'required' => TRUE,
        'wid' => $input_settings['wid'],
      ));
    }
    if ($filters_output != '') {
      $filters_output = '<div class="navigate-filters">'. $filters_output .'</div>';
    }
  }

  if (!isset($input_settings['submit'])) {
    $input_settings['submit'] = 'Save';
  }

  $settings[$input_settings['name']] = isset($settings[$input_settings['name']]) ? $settings[$input_settings['name']] : '';

  $output = '
    <div class="navigate-textarea-outer">
      <div class="navigate-textarea-container">
        <textarea id="'. $input_settings['class'] .'_'. $input_settings['wid'] .'" class="navigate-textarea '. $input_settings['class'] . $select_all_class .' navigate-tooltip" name="'. $input_settings['class'] .'" rows="4" cols="30">'. htmlentities($settings[$input_settings['name']]) .'</textarea>
        '. navigate_tooltip($input_settings['class'] .'_'. $input_settings['wid'], $input_settings['help']) .'
        '. $filters_output .'
        <div class="navigate-submit '. $input_settings['class'] .'_submit_'. $input_settings['wid'] .'">'. t($input_settings['submit']) .'</div>
      </div>
      <input type="hidden" class="navigate-data" value="' . htmlspecialchars(drupal_to_js($input_settings)) . '"></input><
    </div>';
  return $output;
}



/**
 * Return an array of devel menu items, used for menu building by widgets
 */
function navigate_devel_menu($array = array()) {
  if (module_exists('devel') && user_access('access devel information')) {
    $array[] = array(
      'title' => 'Devel settings',
      'path' => 'admin/settings/devel',
      'description' => 'Adjust module settings for devel module');
    $array[] = array(
      'title' => 'Phpinfo()',
      'path' => 'admin/reports/status/php');
    $array[] = array(
      'title' => 'Function reference',
      'path' => 'devel/reference',
      'description' => 'View a list of currently defined user functions with documentation links');
    $array[] = array(
      'title' => 'Reinstall modules',
      'path' => 'devel/reinstall',
      'description' => 'Re-run hook_install() for a given module');
    $array[] = array(
      'title' => 'Reset menus',
      'path' => 'devel/session',
      'description' => 'List the contents of $_SESSION');
    $array[] = array(
      'title' => 'Variable editor',
      'path' => 'devel/variable',
      'description' => 'Edit and delete site variables');
    $array[] = array(
      'title' => 'Session viewer',
      'path' => 'devel/session',
      'description' => 'List the contents of $_SESSION');
  }
  return $array;
}

/**
 * Associate a js callback with clicking content
 */
function navigate_callback_button($settings) {
  $settings['wid'] = isset($settings['wid']) ? $settings['wid'] : '';
  $output = '
    <span id="navigate-button-' . $settings['id'] . '-' . $settings['wid'] . '" class="navigate-button" title="' . $settings['tooltip'] . '">'. $settings['value'] .
    '<input type="hidden" class="navigate-data" value="' . htmlspecialchars(navigate_to_js($settings)) . '"></input></span>';
  return $output;
}


/**
 * Implementation of hook_user().
 */
function navigate_user($op, &$edit, &$account, $category='') {
  switch ($op) {
    case 'delete':
      db_query("DELETE FROM {navigate} WHERE uid = '%d'", $account->uid);
      break;
  }
}


/**
 * Uninstalls a module's widgets on module uninstall
 */
function navigate_uninstall_plugin($plugin) {
  if (module_exists('navigate')) {
    $result = db_query("SELECT * FROM {navigate_widgets} WHERE plugin='%s'", $plugin);
    while ($row = db_fetch_array($result)) {
      db_query("DELETE FROM {navigate_user_settings} WHERE wid = '%d'", $row['wid']);
    }
    db_query("DELETE FROM {navigate_widgets} WHERE plugin='%s'", $plugin);
    //db_query("DELETE FROM {navigate_cache} WHERE wid = '%s'", $module);
  }
}


/**
 * Implementation of hook_theme().
 */
function navigate_theme($existing, $type, $theme, $path) {
  global $theme_key;
  $base = array('file' => 'theme.inc', 'path' => $path . '/theme');
  $theme = array(
    'navigate'                        => $base + array(
      'arguments' => array('navigate' => NULL),
      'template'  => 'navigate',
    ),
    'navigate_admin_settings'         => $base + array('arguments' => array('form' => NULL)),
    'navigate_permission_settings'    => $base + array('arguments' => array('form' => NULL)),
    'navigate_permission_rule'        => $base + array('arguments' => array('form' => NULL)),
    'navigate_fieldset'               => $base + array('arguments' => array('element' => NULL)),
    'navigate_outer'                  => $base + array('arguments' => array('output' => NULL, 'hidden' => NULL)),
    'navigate_settings'               => $base + array(
      'arguments' => array('settings' => NULL),
      'template'  => 'navigate-settings',
    ),
    'navigate_settings_add_widget'    => $base + array('arguments' => array('data' => NULL)),
    'navigate_all_widgets'            => $base + array('arguments' => array('output' => NULL)),
    'navigate_widget'                 => $base + array(
      'arguments' => array('widget' => NULL),
      'pattern'   => 'navigate_widget_',
      'template'  => 'navigate-widget',
    ),
    'navigate_widget_settings'        => $base + array(
      'arguments' => array('settings' => NULL),
      'pattern'   => 'navigate_widget_settings_',
      'template'  => 'navigate-widget-settings',
    ),
    'navigate_widget_settings_button' => $base + array('arguments' => array('wid' => NULL)),
    'navigate_link'                   => $base + array('arguments' => array('menu_item' => NULL)),
  );
  return $theme;
}

/**
 * Duplicate a widget
 */
function navigate_duplicate_widget($wid, $uid) {
  $result = db_query("SELECT * FROM {navigate_widgets} WHERE wid = '%d'", $wid);
  if ($row = db_fetch_array($result)) {
    db_query("INSERT INTO {navigate_widgets} (uid, plugin, weight) VALUES (%d, '%s', %d)", $uid, $row['plugin'], $row['weight']);
    $new_wid = db_last_insert_id('navigate_widgets', 'wid');
    $function = $row['plugin'] .'_navigate_plugin';
    if (function_exists($function)) {
      $function('duplicate', array('wid' => $wid, 'plugin' => $row['plugin'], 'new_wid' => $new_wid, 'uid' => $uid));
    }
  }
  return $new_wid;
}


/**
 * Set value for widget
 */
function navigate_widget_cache_set($wid, $name, $value) {
  $cid = $wid .':'. $name;
  cache_set($cid, $value, 'cache_navigate');
}

/**
 * Get value for widget cache
 */
function navigate_widget_cache_get($wid, $name) {
  $cid = $wid .':'. $name;
  $cache = cache_get($cid, 'cache_navigate');
  if ($cache && isset($cache->data)) {
    return $cache->data;
  }
  return FALSE;
}

/**
 * Save default widget set
 */
function navigate_export($uid) {
  $export = array();
  $widgets = array();
  $result = db_query("SELECT * FROM {navigate_widgets} WHERE uid = '%d' ORDER BY weight ASC", $uid);
  while ($row = db_fetch_array($result)) {
    $settings = array();
    $options = array();
    $settings_results = db_query("SELECT * FROM {navigate_user_settings} WHERE wid = '%d'", $row['wid']);
    while ($setting = db_fetch_array($settings_results)) {
      $settings[] = array(
        'name' => $setting['name'],
        'value' => $setting['value'],
      );
    }
    $function = $row['plugin'] .'_navigate_plugin';
    if (function_exists($function)) {
      $options = $function('export', array('wid' => $row['wid']));
    }
    $widget = array(
      'plugin'   => $row['plugin'],
      'weight'   => $row['weight'],
      'settings' => $settings,
      'options'  => $options,
      );
    $widgets[] = $widget;
  }
  return $widgets;
}

/**
 * Import a widget set
 */
function navigate_import($export, $uid) {
  global $user;
  if (!user_access('navigate administer') && $uid != $user->uid) {
    return FALSE;
  }

  // Delete all current widgets for user
  $result = db_query("SELECT wid FROM {navigate_widgets} WHERE uid = '%d'", $uid);
  while ($row = db_fetch_array($result)) {
    navigate_widget_delete($row['wid'], $uid);
  }

  // Run import
  foreach ($export as $widget) {
    db_query("INSERT INTO {navigate_widgets} (uid, plugin, weight) VALUES (%d, '%s', %d)", $uid, $widget['plugin'], $widget['weight']);
    $wid = db_last_insert_id('navigate_widgets', 'wid');
    if (isset($widget['settings'])) {
      foreach ($widget['settings'] as $setting) {
        db_query("INSERT INTO {navigate_user_settings} (wid, uid, name, value) VALUES (%d, '%d', '%s', '%s')", $wid, $uid, $setting['name'], $setting['value']);
      }
    }
    if (isset($widget['options'])) {
      $function = $widget['plugin'] .'_navigate_plugin';
      if (function_exists($function)) {
        $function('import', array('wid' => $wid, 'uid' => $uid, 'options' => $widget['options']));
      }
    }
  }
}

/**
 * Export a widget set
 */
function navigate_export_widget_set() {
  global $user;
  if (!user_access('navigate administer')) {
    return;
  }
  var_export(navigate_export($user->uid));
  die();
}

/**
 * Import a widget set
 */
function navigate_import_widget_set() {
  if (!user_access('navigate administer')) {
    return;
  }
  global $user;
  $import = eval('return '. $_POST['import'] .';');
  navigate_import($import, $user->uid);
  die();
}