<?php

/**
 * @file
 * Navigate core functions
 */

// Helper function to merge arrays recursively with distinct keys
if (!function_exists('array_merge_recursive_distinct')) {
  function array_merge_recursive_distinct() {
    // Holds all the arrays passed
    $params = & func_get_args();
    // First array is used as the base
    $merged = array_shift($params);
    foreach ($params as $array) {
      foreach ($array as $key => &$value) {
        if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
          $merged[$key] = array_merge_recursive_distinct($merged[$key], $value);
        }
        else {
          $merged[$key] = $value;
        }
      }
    }
    return $merged;
  }
}

/**
 * Process AJAX
 */
function hook_navigate_ajax() {
  if (navigate_check_post()) {
    $op = $_POST['op'];
    unset($_POST['op']);
    $data = $_POST;
    if (isset($data['plugin'])) {
      $plugin = $data['plugin'];
      unset($data['plugin']);
      $function = $plugin . '_navigate_ajax';
      if (module_exists($plugin) && function_exists($function)) {
        $function($op, $data);
      }
    }
    else {
      $function = 'navigate_' . $op;
      if (function_exists($function)) {
        $function();
      }
    }
  }
}

function navigate_get_form() {
  $form_id = $_POST['form'];
  switch ($form_id) {
    case 'navigate_admin_settings':
      include_once dirname(__FILE__) . '/admin.inc'; break;
  }
  navigate_json(array('content' => drupal_get_form($form_id)));
}

function hook_navigate_permissions() {
  $defaults = array(
    'settings',
    'view',
    'widget add',
    'widget title',
  );
  $permissions = array(
    'navigate' => array_merge($defaults, array(
      '#title' => 'Navigate',
      'administer',
      'widget remove',
      'widget sort',
    ))
  );
  $hook = 'navigate_permissions';
  foreach (module_implements($hook) as $module) {
    $function = $module . '_' . $hook;
    $plugin_permission = (array) $function();
    foreach ($plugin_permission as $plugin => $perms) {
      $plugin_permission[$plugin] = array_merge($defaults, $perms);
      $function = $module .'_navigate_plugin';
      if (function_exists($function)) {
        $settings = $function('settings');
        if (empty($settings)) unset($plugin_permission[$plugin][array_search('settings', $plugin_permission[$plugin])]);
      }
    }
    $permissions = array_merge_recursive_distinct($permissions, $plugin_permission);
  }
  return $permissions;
}
 
/**
 * Generate a list of available plugins that creates a widget
 */
function hook_navigate_plugin() {
  $settings = array(
    'general' => array(
      'title'       => 'General',
      'description' => t('Various Navigate Settings'),
      'section'     => array(
        'performance' => array(
          'header'  => 'Performance',
          'content' => 'This is where performance settings will be.',
        )
      ),
    ),
    'widget' => array(
      'title'       => t('Widgets'),
      'description' => t('Add/Remove Widgets'),
      'section'    => array(
        'add' => array(
          'header'  => t('Add Widget'),
          'content' => '',
          'empty'   => t('There are currently no plugins enabled. Please visit the "Navigate - Plugins" section on the the !module page to enable them.', array('!module' => l('module settings', 'admin/build/modules'))),
          'plugins' => array(),
        ),
        'remove' => array(
          'header'  => t('Remove Widget'),
          'content' => t('To remove a widget, drag it away from the Navigate dock. A droppable area will appear over the page on the right. Drop the widget there to remove it. Once a widget is removed, it no longer exists and cannot be retrieved.'),
        )
      ),
    )
  );
  // Create Add Widget section
  foreach (module_implements('navigate_plugin') as $module) {
    $function = $module .'_navigate_plugin';
    $settings['widget']['section']['add']['content'] .= theme('navigate_settings_add_widget', $function('widget_add'));
  }
  if (!empty($settings['widget']['section']['add']['content'])) {
    $settings['widget']['section']['add']['content'] = '<ul id="navigate-plugin-list">' . $settings['widget']['section']['add']['content'] . '</ul>';
  }
  // Administrative Settings
  if (navigate_user_access('administer')) {
    $settings['admin'] = array(
      'title' => 'Administration',
      'description' => t('Administrative Settings'),
      'section' => array(
        'roles' => array(
          'header'  => 'Users &amp; Roles',
          'content' => navigate_build_defaults_admin(),
        )
      ),
    );
  }
  // Process any tab callback data
  foreach ($settings as $tab => $data) {
    if ((isset($data['page callback']) && isset($data['page arguments'])) || isset($data['js callback'])) {
      $callbacks = array(
        'jsCallback' => $data['js callback'],
        'pageCallback' => array_merge(array('op' => $data['page callback']), $data['page arguments']),
      );
      $settings[$tab]['data'] = '<input type="hidden" class="navigate-data" value="' . htmlspecialchars(navigate_to_js($callbacks)) . '"></input>';
    }
  }
  return $settings;
}

function navigate_user_access($permission, $plugin = 'navigate', $account = NULL, $reset = TRUE) {
  global $_navigate, $user;
  static $perm = array();
  if ($reset) $perm = array();
  if (!isset($account)) $account = $user;
  if ($account->uid == 1) return TRUE;
  if (!isset($perm[$account->uid])) {
    // Initialize base permissions (no access)
    $base_perms = hook_navigate_permissions();
    // Process Rules
    $perms = array();
    $query = db_query('SELECT * FROM {navigate_permissions} WHERE status = 1 ORDER BY weight ASC');
    while ($rule = db_fetch_object($query)) {
      $user_in_rule = FALSE;
      $roles = array();
      $users = array();
      $ids = explode(',', $rule->ids);
      foreach ($ids as $id) {
        $id = explode(':', $id);
        switch ($id[0]) {
          case 'rid':
            $roles[] = $id[1];
            break;
          case 'uid':
            $users[] = $id[1];
            break;
        }
      }
      // Process roles in rule first
      foreach($roles as $rid) {
        if (isset($account->roles[$rid])) {
          $user_in_rule = TRUE;
          break;
        }
      }
      // Process users next
      foreach($users as $uid) {
        if ($account->uid == $uid) {
          $user_in_rule = TRUE;
          break;
        }
      }
      // Only continue processing this rule if user is inside a role or specified specifically
      if ($user_in_rule) {
        switch($rule->type) {
          case 'all':
            foreach($base_perms as $base_plugin => $base_perms) {
              foreach($base_perms as $base_perm) {
                $perms[$base_plugin][$base_perm] = TRUE;
              }
            }
            break;
          case 'none':
            foreach($base_perms as $base_plugin => $base_perms) {
              foreach($base_perms as $base_perm) {
                if (isset($perms[$base_plugin][$base_perm])) unset($perms[$base_plugin][$base_perm]);
              }
            }
            break;
          case 'custom':
            $rule->permissions = unserialize($rule->permissions);
            foreach($rule->permissions as $key => $value) {
              $key = explode(':', $key);
              $custom_plugin = $key[0];
              $custom_perm = str_replace('_', ' ', $key[1]);
              if ($value) {
                $perms[$custom_plugin][$custom_perm] = TRUE;
              }
              else {
                if (isset($perms[$custom_plugin][$custom_perm])) unset($perms[$custom_plugin][$custom_perm]);
              }
            }
            break;
        }
      }
    }
    $perm[$account->uid] = $perms;
  }
  return isset($perm[$account->uid][$plugin][$permission]);
}


function navigate_add_js($data, $module = 'navigate') {
  $base_path = $_SERVER['DOCUMENT_ROOT'] . base_path();
  $module_path = drupal_get_path('module', $module);
  if (!is_array($data)) {
    $data = array($data);
  }
  foreach ($data as $file) {
    if (file_exists($base_path . $module_path . '/' . $file)) {
      $file = $module_path . '/' . $file;
    }
    else if (file_exists($base_path . $module_path . '/js/' . $file)) {
      $file = $module_path . '/js/' . $file;
    }
    else {
      continue;
    }
    drupal_add_js($file, $module == 'navigate' ? 'core' : 'plugin', 'navigate');
  }
}

function navigate_add_css($file, $module = 'navigate', $compress = FALSE) {
  $file_info = pathinfo($file);
  $file_name =  basename($file, '.' . $file_info['extension']);
  $base_path = $_SERVER['DOCUMENT_ROOT'] . base_path();
  $module_path = drupal_get_path('module', $module);
  
  if (file_exists($base_path . $module_path . '/' . $file)) {
    $file_path = '/';
  }
  else if (file_exists($base_path . $module_path . '/css/' . $file)) {
    $file_path = '/css/';
  }
  else {
    return FALSE;
  }
  
  file_check_directory(file_create_path('css'), FILE_CREATE_DIRECTORY);
  $css_path = file_create_path('css/navigate');
  file_check_directory($css_path, FILE_CREATE_DIRECTORY);
  
  $output_file = $css_path .'/'. $file_name . '_' . md5_file($base_path . $module_path . $file_path . $file) . '.css';
  if (!file_exists($base_path . $output_file)) {
    require_once('lessphp/lessc.inc');
    $less = new lessc();
    $less->importDir = $base_path . $module_path . $file_path;
    $contents = drupal_load_stylesheet($base_path . $module_path . $file_path . $file, false);
    _drupal_build_css_path(NULL, base_path() . $module_path . $file_path);
    // Prefix all paths within this CSS file, ignoring external and absolute paths.
    $data = preg_replace_callback('/url\([\'"]?(?![a-z]+:|\/+)([^\'")]+)[\'"]?\)/i', '_drupal_build_css_path', $contents);
    $data = $less->parse($data);
    if ($compress) {
      require_once('iceycss/iceycss.inc');
      $iceycss = new iceycss();
      $data = $iceycss->compress($data);
    }
    file_save_data($data, $output_file, FILE_EXISTS_REPLACE);
  }
  drupal_add_css($output_file, $module == 'navigate' ? 'module' : 'plugin');
}

/**
 * Menu callback; Retrieve a JSON object containing autocomplete suggestions for existing users and roles
 */
function navigate_autocomplete() {
  $json = array();
  if (navigate_check_post()) {
    switch($_POST['op']) {
      case 'prepopulate':
        $roles = array();
        $users = array();
        $tokens = explode(',', rawurldecode($_POST['prepopulate']));
        foreach ($tokens as $token) {
          $token = explode(':', $token);
          $type = $token[0];
          $id = $token[1];
          switch ($type) {
            case 'rid':
              $result = db_query("SELECT rid, name FROM {role} WHERE rid = '%d'", $id);
              while ($role = db_fetch_object($result)) {
                $roles[] = array('name' => $role->name, 'id' => 'rid:' . $role->rid, 'category' => 'Roles');
              }
              break;
            case 'uid':
              $result = db_query("SELECT uid, name FROM {users} WHERE uid = '%d'", $id);
              while ($user = db_fetch_object($result)) {
                $users[] = array('name' => $user->name, 'id' => 'uid:' . $user->uid, 'category' => 'Users');
              }
              break;
          }
        }
        $json = array_merge($roles, $users);
        break;
      default:
        $term = $_POST['term'];
        // Process existing tokens
        $existing_roles = array();
        $existing_users = array();
        $existingTokens = explode(',', rawurldecode($_POST['existing']));
        foreach ($existingTokens as $token) {
          $token = explode(':', $token);
          $type = $token[0];
          $id = $token[1];
          switch ($type) {
            case 'rid':
              $existing_roles[] = $id;
              break;
            case 'uid':
              $existing_users[] = $id;
              break;
          }
        }
        $existing_roles_sql = '';
        if (!empty($existing_roles)) {
          $existing_roles_sql = " AND rid NOT IN (" . db_placeholders($existing_roles, 'int') . ")";
        }
        $existing_users_sql = '';
        if (!empty($existing_users)) {
          $existing_users_sql = " AND uid NOT IN (" . db_placeholders($existing_users, 'int') . ")";
        }
        
        if ($term) {
          // Roles
          $roles = array();
          if (is_numeric($term)) {
            $result = db_query_range("SELECT rid, name FROM {role} WHERE rid LIKE '%%%d%%'" . $existing_roles_sql, array($term, $existing_roles), 0, 10);
          }
          else {
            $result = db_query_range("SELECT rid, name FROM {role} WHERE LOWER(name) LIKE LOWER('%%%s%%')" . $existing_roles_sql, array($term, $existing_roles), 0, 10);
          }
          while ($role = db_fetch_object($result)) {
            // Ignore default Drupal roles
            if ($role->rid == 1 || $role->rid == 2) continue;
            $roles[] = array('name' => $role->name, 'id' => 'rid:' . $role->rid, 'description' => '(rid: ' . $role->rid . ')', 'category' => 'Roles');
          }
          // Users
          $users = array();
          if (is_numeric($term)) {
            $result = db_query_range("SELECT uid, name, status FROM {users} WHERE uid LIKE '%%%d%%'" . $existing_users_sql, array($term, $existing_users), 0, 10);
          }
          else {
            $query = "SELECT uid, name, status FROM {users} WHERE LOWER(name) LIKE LOWER('%%%s%%')" . $existing_users_sql;
            $result = db_query_range($query, array($term, $existing_users), 0, 10);
          }
          while ($user = db_fetch_object($result)) {
            // Ignore site admin, always has acess
            if ($user->uid == 1) continue;
            $users[] = array('name' => $user->name, 'id' => 'uid:' . $user->uid, 'description' => '(' . ($user->status ? '' : 'inactive, ') . 'uid: ' . $user->uid . ')', 'category' => 'Users');
          }
          $json = array_merge($roles, $users);
        }
        break;
    }
  }
  navigate_json($json);
}

function navigate_file_info($file, $type = 'navigate') {
  $file_info = pathinfo($file);
  // Fix pathinfo['filename'] for PHP < 5.2
  if (!isset($file_info['filename'])) $file_info['filename'] = basename($file, '.' . $file_info['extension']);
  $file_info['basepath'] = $_SERVER['DOCUMENT_ROOT'] . base_path();
  if ($type == 'filedir') {
    $file_info['path'] = file_directory_path();
  }
  else {
    $file_info['path'] = drupal_get_path('module', $type);
  }
  $subdirs = array('/', '/css/', '/css/theme/', '/includes/', '/js/');
  $exists = FALSE;
  foreach ($subdirs as $subdir) {
    if (file_exists($file_info['basepath'] . $file_info['path'] . $subdir . $file_info['basename'])) {
      $file_info['path'] .= $subdir;
      $exists = TRUE;
      break;
    }
  }
  if (!$exists) return FALSE;
  $file_info['file'] = $file_info['basepath'] . $file_info['path'] . $file_info['basename'];
  return $file_info;
}

function navigate_jquery_ui_process($variables, $image_path) {
  foreach($variables as $attribute => $value) {
    switch(TRUE) {
      case preg_match('/^ui-icon/', $attribute):
        $output_file = $image_path . '/ui_icon_' . $value . '.png';
        if (!file_exists($output_file)) {
          $contents = file_get_contents('http://jqueryui.com/themeroller/images/?new=' . $value . '&w=256&h=240&f=png&fltr[]=rcd|256&fltr[]=mask|icons/icons.png');
          file_save_data($contents, $output_file, FILE_EXISTS_REPLACE);
        }
        break;
    }
  }
}

function navigate_theme_load($theme, $plugin = 'navigate', $templates = array(), $compress = TRUE) {
  require_once('lessphp/lessc.inc');
  $less = new lessc();
  
  $theme_name = $theme;
  
  $theme = navigate_file_info($theme . '.theme', $plugin);
  if ($theme) {
    file_check_directory(file_create_path('css'), FILE_CREATE_DIRECTORY);
    $css_path = file_create_path('css/navigate');
    file_check_directory($css_path, FILE_CREATE_DIRECTORY);
    file_check_directory(file_create_path('navigate'), FILE_CREATE_DIRECTORY);
    $jqueryui_image_path = file_create_path('navigate/images');
    file_check_directory($jqueryui_image_path, FILE_CREATE_DIRECTORY);
    $output_file = $css_path .'/'. $theme_name . '_' . md5_file($theme['file']) . '.css';
    if (!file_exists($theme['basepath'] . $output_file)) {
      $theme['contents'] = file_get_contents($theme['file']);
      $theme['contents'] .= '@JQUERY_UI_IMAGES: "' . base_path() . $jqueryui_image_path . "\";\n";
      $theme['contents'] .= '@THEME_PATH: "' . base_path() . $theme['path'] . "\";\n";
      $theme['variables'] = $less->parse($theme['contents'], 'variables');
      $jqueryui = array();
      foreach($theme['variables'] as $attribute => $value) {
        if (preg_match("/^ui/", $attribute)) {
          $jqueryui[$attribute] = $value;
        }
      }
      navigate_jquery_ui_process($jqueryui, $jqueryui_image_path);    
      if (empty($templates)) {
        $templates = array(
          'jquery.ui.tpl.css',
          'navigate.tpl.css',
          'navigate.settings.tpl.css',
        );
      }
      foreach($templates as $template) {
        $template = navigate_file_info($template, $plugin);
        $theme['contents'] .= drupal_load_stylesheet($template['file'], FALSE);
      }
      $less->importDir = $theme['basepath'] . $theme['path'];
      $theme['contents'] = $less->parse($theme['contents']);
      _drupal_build_css_path(NULL, base_path() . $theme['path']);
      // Prefix all paths within this CSS file, ignoring external and absolute paths.
      $data = preg_replace_callback('/url\([\'"]?(?![a-z]+:|\/+)([^\'")]+)[\'"]?\)/i', '_drupal_build_css_path', $theme['contents']);
      if ($compress) {
        require_once('iceycss/iceycss.inc');
        $iceycss = new iceycss();
        $data = $iceycss->compress($data);
      }
      file_save_data($data, $output_file, FILE_EXISTS_REPLACE);
    }
    drupal_add_css($output_file, $module == 'navigate' ? 'navigate' : 'plugin');
  }  
}

function navigate_less_variables($file, $module = 'navigate') {
  $file = navigate_file_info($file);
  $contents = drupal_load_stylesheet($file['file'], false);
  _drupal_build_css_path(NULL, base_path() . $file['path']);
  // Prefix all paths within this CSS file, ignoring external and absolute paths.
  $data = preg_replace_callback('/url\([\'"]?(?![a-z]+:|\/+)([^\'")]+)[\'"]?\)/i', '_drupal_build_css_path', $contents);
  require_once('lessphp/lessc.inc');
  $less = new lessc();
  $less->importDir = $file['basepath'] . $file['path'];
  $data = $less->parse($data, 'variables');
  if (navigate_check_post() && $_POST['op'] == 'less_variables') {
    navigate_json(array('success' => TRUE, 'data' => $data));
  }
  else {
    return $data;
  }
}

function navigate_check_post() {
  $post = FALSE;
  if (!empty($_POST)) {
    $post = TRUE;
    foreach ($_POST as $key => $value) {
      $match = '';
      if (is_array($value)) continue;
      if (preg_match('/^\(bool\)(\d)/', $value, $match) > 0) {
        $_POST[$key] = (bool) $match[1];
      }
    }
  }
  return $post;
}

function navigate_json($var = NULL) {
  // We are returning JavaScript, so tell the browser.
  drupal_set_header('Content-Type: text/javascript; charset=utf-8');
  if (isset($var)) {
    echo navigate_to_js($var);
  }
}

function navigate_settings_get($name = FALSE, $default = NULL, $refresh = FALSE) {
  global $_navigate;
  if (!isset($_navigate['settings']) || $refresh) $_navigate['settings'] = variable_get('navigate', array());
  return $name ? isset($_navigate['settings'][$name]) ? $_navigate['settings'][$name] : $default : $_navigate['settings'];
}

function navigate_settings_set($name, $value, $save = TRUE) {
  global $_navigate;
  $_navigate['settings'][$name] = $value;
  if ($save) variable_set('navigate', $_navigate['settings']);
  return $_navigate['settings'];
}

// Compare weights
function navigate_sort_weights($a, $b) {
  $a_weight = (is_array($a) && isset($a['weight'])) ? $a['weight'] : 0;
  $b_weight = (is_array($b) && isset($b['weight'])) ? $b['weight'] : 0;
  if ($a_weight == $b_weight) {
    return 0;
  }
  return ($a_weight < $b_weight) ? -1 : 1;
}

function navigate_to_js($var) {
  // // Use PHP's json_encode function if it exists
  // if (function_exists('json_encode')) {
  //   return str_replace(array('<', '>', '&'), array('\u003c', '\u003e', '\u0026'), json_encode($var));
  // }
  // Or use our own (drupal_to_js() with a few modifications)
  switch (gettype($var)) {
    case 'boolean':
      return $var ? 'true' : 'false'; // Lowercase necessary!
    case 'integer':
    case 'double':
      return $var;
    case 'resource':
    case 'string':
      return '"'. str_replace(array("\n", "\r", "<", ">", "&", "'", '"'),
                              array('\n', '\r', '\u003c', '\u003e', '\u0026', '\u0027', '\u0022'),
                              $var) .'"';
    case 'array':
      // Arrays in JSON can't be associative. If the array is empty or if it
      // has sequential whole number keys starting with 0, it's not associative
      // so we can go ahead and convert it as an array.
      if (empty ($var) || array_keys($var) === range(0, sizeof($var) - 1)) {
        $output = array();
        foreach ($var as $v) {
          $output[] = navigate_to_js($v);
        }
        return '[ '. implode(', ', $output) .' ]';
      }
      // Otherwise, fall through to convert the array as an object.
    case 'object':
      $output = array();
      foreach ($var as $k => $v) {
        $output[] = navigate_to_js(strval($k)) .': '. navigate_to_js($v);
      }
      return '{ '. implode(', ', $output) .' }';
    default:
      return 'null';
  }
}

function navigate_user_load($uid = FALSE, $refresh = FALSE) {
  global $_navigate, $user;
  if (!$uid) $uid = $user->uid;
  if ($uid) {
    if (!isset($_navigate['users'][$uid]) || $refresh) {
      $data = db_result(db_query("SELECT data FROM {navigate} WHERE uid = '%d'", $uid));
      if ($data) {
        $_navigate['users'][$uid] = unserialize($data);
        return $_navigate['users'][$uid];
      }
      else {
        // Initialize default widgets
        if (!isset($_navigate['users']['default']['widgets'])) {
          foreach (module_implements('navigate_plugin') as $module) {
            $function = $module .'_navigate_plugin';
            $widget = $function('widget_default');
            if (!empty($widget)) $_navigate['users']['default']['widgets'][] = $widget;
          }
          if (!empty($_navigate['users']['default']['widgets'])) $_navigate['users']['default']['settings']['widgetIndex'] = count($_navigate['users']['default']['widgets']) - 1;
        }
        $_navigate['users'][$uid] = $_navigate['users']['default'];
        navigate_user_save($uid);
        return $_navigate['users'][$uid];
      }
    }
    else {
      return $_navigate['users'][$uid];
    }
  }
  return FALSE;
}

function navigate_user_save($uid = FALSE) {
  global $_navigate, $user;
  if (!$uid) $uid = $user->uid;
  if ($uid && isset($_navigate['users'][$uid])) {
    $data = serialize($_navigate['users'][$uid]);
    db_query("UPDATE {navigate} SET data = '%s' WHERE uid = '%d'", $data, $uid);
    if (!db_affected_rows()) {
      @db_query("INSERT INTO {navigate} (uid, data) VALUES ('%s', '%d')", $uid, $data);
      return db_affected_rows() > 0 ? TRUE : FALSE;
    }
    return TRUE;
  }
  return FALSE;
}

/**
 * Save a user variable
 */
function navigate_variable_set($name = FALSE, $value = FALSE, $uid = FALSE, $wid = FALSE) {
  global $_navigate, $user;  
  $ajax = FALSE;
  $return = FALSE;
  $save = FALSE;
  // Overwrite variables with POST data
  if (navigate_check_post()) {
    $ajax = TRUE;
    $jsonData = array();
    $name = $_POST['name'];
    $value = $_POST['value'];
    $wid = $_POST['wid'];
    $uid = $_POST['uid'];
  }
  // Set the user to logged user if not specified
  if (!$uid) $uid = $user->uid;  
  // Ensure user exists and the name is valid
  if (isset($_navigate['users'][$uid]) && $name) {
    // Save widget setting
    if ($wid) {
      // Ensure this widget exists
      if (isset($_navigate['users'][$uid]['widgets'][$wid])) {
        $_navigate['users'][$uid]['widgets'][$wid]['settings'][$name] = $value;
        $save = TRUE;
      }
    }
    // Set user setting
    else {
      $_navigate['users'][$uid]['settings'][$name] = $value;
      $save = TRUE;
    }
  }  
  if ($save) {
    $return = navigate_user_save($uid);
  }
  if ($ajax) {
    navigate_json(array('success' => $return));
  }
  else {
    return $return;
  }
}


/**
 * Get a user variable
 */
function navigate_variable_get($name = FALSE, $uid = FALSE, $wid = FALSE, $refresh = FALSE) {
  global $_navigate, $user;
  // Set the user to logged user if not specified
  if (!$uid) $uid = $user->uid;
  // Refresh from database if needed
  if ($refresh) {
    navigate_user_load($uid, TRUE);
  }
  // Ensure user exists and the name is valid
  if (isset($_navigate['users'][$uid]) && $name) {
    // Get widget setting
    if ($wid) {
      // Ensure this widget exists
      if (isset($_navigate['users'][$uid]['widgets'][$wid])) {
        return $_navigate['users'][$uid]['widgets'][$wid]['settings'][$name];
      }
    }
    // Get user setting
    else {
      return $_navigate['users'][$uid]['settings'][$name];
    }
  }
}


/**
 * Delete a user variable
 */
function navigate_variable_delete($name, $wid=0) {
  global $user;
  db_query("DELETE FROM {navigate_user_settings} WHERE name = '%s' AND wid = '%d' AND uid = '%d'", $name, $wid, $user->uid);
}
