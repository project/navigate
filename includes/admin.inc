<?php

/**
 * Defines admin settings form
 */
function navigate_admin_settings($form_state) {
  navigate_add_js('navigate.admin.js');
  navigate_add_css('navigate.admin.css');
  $form = array();
  $form['navigate'] = array(
    '#tree' => TRUE,
  );
  $f =& $form['navigate'];
  
  // Page Visibility
  $f['page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page specific visibility settings'),
    '#collapsible' => TRUE,
  );
  $f['page']['visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Show Navigate on specific pages'),
    '#options' =>array(t('Show on every page except the listed pages.'), t('Show on only the listed pages.'), t('Show if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).')),
    '#default_value' => navigate_settings_get('visibility', 0),
  );
  $f['page']['pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => navigate_settings_get('pages', '*imce*'),
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %node for a node page and %node-wildcard for every node page. %front is the front page.", array('%node' => 'node/1', '%node-wildcard' => 'node/*', '%front' => '<front>')) . ' ' .
                      t('If the PHP-mode is chosen, enter PHP code between %php. Note that executing incorrect PHP-code can break your Drupal site.', array('%php' => '<?php ?>')),
  );
  
  // Performance
  $f['performance'] = array(
    '#type' => 'fieldset',
    '#title' => t('Performance'),
    '#collapsible' => TRUE,
    '#tree' => TRUE,
  );
  $f['performance']['ajax'] = array(
    '#type' => 'checkbox',
    '#title' => t('AJAX Optimization'),
    '#default_value' => navigate_settings_get('render_ajax', 1),
    '#description' => t('If enabled, Navigate will only load necessary HTML upon each page load. This will dramatically reduce the HTML output of Navigate and increase page load response time. Useful if users have lots of widgets and/or settings available to configure.'),
  );
  $f['performance']['user_override'] = array(
    '#type' => 'checkbox',
    '#title' => t('Let users override these settings'),
    '#description' => t('If enabled, this will present the above options in Settings > General > Performance.'),
    '#default_value' => navigate_settings_get('render_user_override', 0),
  );
  
  $f['rendering'] = array(
    '#type' => 'fieldset',
    '#title' => t('Rendering'),
    '#collapsible' => TRUE,
    '#tree' => TRUE,
  );
  $f['rendering']['widget_display'] = array(
    '#type' => 'radios',
    '#title' => t('Widget Display Method'),
    '#default_value' => navigate_settings_get('widget_display', 'slider'),
    '#options' => array(
      'slider' => t('Sliding Widgets'),
      'stacked' => t('Stacked Widgets (Navigate 1.0)'),
    ),
    '#description' => t('<dl><dt><strong>Sliding Widgets:</strong></dt><dd>This new display provides a fixed and automatically adjusting height "Navigation" style bar on your site. Imploring a new method which allows easier and constant access to all widgets.</dd><dt><strong>Stacked Widgets (Navigate 1.0):</strong></dt><dd>This method is from the previous version of Navigate. Navigate will maintain it\'s relative position on your site, however the overall height of Navigate will vary greatly depending on each widget\'s height requirement (which can dynamically change). This method also severely reduces the effect of AJAX Optimization (above) as Navigate must load all widgets, whether or not they are needed.</dd></dl>'),
  );
  
  $f['rendering']['user_override'] = array(
    '#type' => 'checkbox',
    '#title' => t('Let users override these settings'),
    '#description' => t('If enabled, this will present the above options in Settings > General > Rendering.'),
    '#default_value' => navigate_settings_get('render_user_override', 0),
  );
  
  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration') );
  $form['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Reset to defaults') );
  
  return $form;
}

function navigate_flatten_array($array = NULL) {
  $output = array();
  foreach($array as $key => $value) {
    $key = str_replace(array('_', ' '), '-', $key);
    if(is_array($value)) {
      $array2 = navigate_flatten_array($value);
      foreach($array2 as $key2 => $value2) {
        $output[$key . '-' . $key2] = $value2;
      }
    }
    else {
      $output[$key] = $value;
    }
  }
  return $output;
}

function navigate_admin_settings_submit($form, &$form_state) {
  $values = $form_state['values']['navigate'];
  drupal_set_message('<pre>' . print_r($values, TRUE) . '</pre>');
  drupal_set_message('<pre>' . print_r(navigate_flatten_array($values), TRUE) . '</pre>');
  switch ($form_state['clicked_button']['#value']) {
    case 'Save configuration':
      drupal_set_message(t('Navigate settings have been saved.'));
      break;
    case 'Reset to defaults':
      drupal_set_message(t('Navigate settings have been reset.'));
      $values['visibility'] = 0;
      $values['pages'] = '*imce*';
      $values['render'] = 'slider';
      $values['render_ajax'] = 1;
      $values['render_user_override'] = 0;
      break;
  }
  // navigate_settings_set('visibility', $values['visibility'], FALSE);
  // navigate_settings_set('pages', $values['pages']);
  // navigate_settings_set('render', $values['render']);
  // navigate_settings_set('render_ajax', $values['render_ajax']);
  // navigate_settings_set('render_user_override', $values['render_user_override']);
}
