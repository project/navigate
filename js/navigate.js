
// Reference Navigate.jQuery with $ and Navigate with $$
(function($, $$) {
	$.extend(Navigate, {
	  
//  *******************************************************
//  Navigate Defaults

    version: '2.0-beta1',
    plugins: {},
    settings: {
      loaded: false,
      mouseScrollSpeed: 5,
      queue: 0,
      slider: {
        autoHeight: false,
        closedHeight: 32,
        currentWidget: {
          wid: 0
        },
        previousWidget: {
          animated: false,
          wid: 0        
        },
        sorting: false
      }
    },
    user: {},
    widgets: {},

//  *******************************************************
//  Navigate Methods
   
    // Send request to Navigate processing function
    ajax: function(ajax) {
      // Allow plugins to modify ajax data before processing it
      $$.execute({ event: 'ajaxBefore', data: ajax });
      ajax = $.extend(true, {}, {
        async: true,
        data:     {
          'wid':      false,
          'return':   $$.settings.destination
        },
        dataType: 'json',
        error:    function(){},
        success:  function(){},
        type:     'POST',
        url:      Drupal.settings.basePath + 'ajax/navigate'
      }, ajax);
      // URL encode values
      $.each(ajax.data, function(key, value){
        ajax.data[key] = $$.urlEncode(value);
      });
      $$.queueAdd();
      $.ajax({
          async: ajax.async,
          data: ajax.data,
          dataType:   ajax.dataType,
          error: function(json) {
            $$.queueSubtract();
            ajax.error(json);
          },
          success: function(json) {
            $$.queueSubtract();
            ajax.success(json);
          },
          type: ajax.type,
          url: ajax.url
      });
      $$.execute({ event: 'ajaxAfter', data: ajax });
    },    
    /**
     * Allow plugins to dynamically change the title of any widget.
     *
     * @param wid
     *   An integer containing the ID of the desired widget.
     * @param title
     *   A string containing the new title to be set.
     *
     * @return
     *   The previous widget's title, or FALSE if the widget does not exist.
     */
    changeTitle: function(wid, title) {
      var widgetTitle = $$.getWidget(wid).find('.navigate-widget-title');
      if (widgetTitle.length) {
        var previousTitle = $(widgetTitle).text();
        widgetTitle.animate({ opacity: 0 }, { queue: false, duration: 400, complete: function(){
          widgetTitle.text(title);
          widgetTitle.animate({ opacity: 1 }, { queue: false, duration: 400});
        }});
        if ($$.settings.customize) {
          $(widgetTitle).addClass('customize');
        }
        else {
          $(widgetTitle).removeClass('customize');
        }
        $$.execute({ event: 'changeTitle' });
        return previousTitle;
      }
      else {
        return false;
      }
    },
    closest: function(selector, context) {
      if (context == undefined) context = '#navigate';
      var parent = $(context);
      if (parent.length) {
        do {
          if (parent.is(selector)) {
            return parent;
          }
          else if (parent.find(selector).length) {
            return parent.find(selector);
          }
          else {
            if (parent.is('#navigate')) {
              return false;
            }
            parent = parent.parent();
          }
        } while (true);
      }
      else {
        return false;
      }
    },
    customizeTitle: function(element) {
      var parent = $(element).parent();
      var wid = $(parent).find('.navigate-title-wid').val();
      var oldTitle = $(element).text();
      var newTitle = '';
      $(element).hide();
      var input = $(parent).find('.navigate-title-input');
      if (!input.length) {
        input = $('<input type="text"/>').addClass('navigate-title-input').val(oldTitle);
        $(parent).append(input);
      }
      $(input).focus().select();
      $(input).blur(function(){
        $(element).show();
        $(this).remove();
      });
      $(input).bind('keyup', function(e) {
        // Enter or Esc pressed
        if (e.which == 13 || e.keyCode == 13 || e.which == 27 || e.keyCode == 27) {
          newTitle = $(this).val();
          if (newTitle == '') { newTitle = oldTitle; }
          if (oldTitle != newTitle) {
            // Save new title
            if (e.which == 13 || e.keyCode == 13) {
              $$.variableSet('title', newTitle, wid);
            }
            // Cancel
            else if (e.which == 27 || e.keyCode == 27) {
              newTitle = oldTitle;
            }
          }
          // Show Title
          $(element).text(newTitle).show();
          $(input).remove();
        }
      });
    },
    /**
     * Executes events through out Navigate and plugins. If no data is passed, then this method will execute all events found
     * in the Navigate namespace (this includes plugins). To improve performance, it is strongly recommended that data be
     * passed to restrict the scope(s) and event(s) whenever possible. This allows only the necessary event(s) to be executed.
     * See examples below.
     *
     * @param {object} data
     *   An object literal containing key/value pairs of event attributes:
     *   - event:         A string or an array of strings containing the event(s) to invoke. (default: 'all')
     *     - all:         All events found within the given scope are invoked. This option does not work inside an array.
     *     - {event}:     A specific event to invoke. (replace {event} with the name of the event)
     *   - context:       A DOM Element, Document or jQuery to use as context. (default: $('#navigate'))
     *   - data:          A string, array or object used in relaying a method's parameters.
     *   - scope:         A string or an array of strings containing the scope(s) to limit executed event(s) from. (default: 'all')
     *     - all:         The scope is widened to Navigate and all plugins. This option does not work inside an array.
     *     - navigate:    The scope is restricted to Navigate only.
     *     - {plugin}:    The scope is resitricted to a specific plugin. (replace {plugin} with the name of the plugin)
     *
     * @param {object} history 
     *   (optional) An object literal containing key/value pairs of the previous event data. This should only be passed if
     *   using the Navigate.execute method while inside an event. This parameter, for all intents and purposes, allows
     *   events to become chained. History is always prepended with the previous event's data.
     *   To reference the context from the previous event data, use: data.history[0].context.
     *   To reference the context from the first event data, use: data.history[data.history.length - 1].context.
     *
     * Example usage:
     * @code
     *   // Attach the bindTreeview event from the navigate_menu plugin and attach it to a particular widget's content.
     *   // From navigate_menu/navigate_menu.js:
     *   $$.execute({ event: 'bindTreeview', plugin: 'navigate_menu', context: $$.getWidget('navigate_menu') });
     *
     *   // Call bindWidget after a new widget is created. This will invoke Navigate's bindWidget and all plugins that have a
     *   // bindWidget event defined.
     *   $$.execute({  event: 'bindWidget' });
     * @endcode
     *
     * @see navigate_menu/navigate_menu.js
     *   For a detailed example on how to implement events.
     */
    execute: function(data, history) {
      // Merge history subprocesses
      var oldHistory = [];
      var newHistory = [];
      if (history && history != undefined) {
        var historyClone = $.extend({}, history);
        oldHistory = historyClone.history;
        delete historyClone.history;
        newHistory.unshift(historyClone);
        newHistory = newHistory.concat(oldHistory);
      }
      
      // Extend data with defaults and history
      var data =	$.extend(true, {}, {
        event:   'all',
        context:  $('#navigate'),
        scope:   'all'
      }, data);
      // Update the new history
      data.history = newHistory;
      
      // Restrict the context within Navigate's DOM
      if (!data.context) data.context = $('#navigate');
      if (!$(data.context).is('#navigate')) {
        data.context = $($(data.context), $('#navigate'));
      }
      
      // Clean up array or convert string into array.
      if ($$.isArray(data.scope) || $$.isObject(data.scope)) {
        var scopeArray = [];
        $.each(data.scope, function(index, scope){
          // Ignore all, blank and undefined scopes because this is an array
          if (scope != 'all' && scope != '' && scope != undefined) {
            scopeArray.push(scope);
          }
        });
        data.scope = scopeArray;
      }
      else {
        data.scope = [(data.scope == '' || data.scope == undefined ? 'all' : data.scope)];
      }
      if (data.scope.length < 1) data.scope = ['all'];
      
      // Clean up array or convert string into array
      if ($$.isArray(data.event) || $$.isObject(data.event)) {
        var eventArray = [];
        $.each(data.event, function(index, event){
          // Ignore all, blank and undefined events because this is an array
          if (event != 'all' && event != '' && event != undefined) {
            eventArray.push(event);
          }
        });
        data.event = eventArray;
      }
      else {
        data.event = [(data.event == '' || data.event == undefined ? 'all' : data.event)];
      }
      if (data.event.length < 1) data.event = ['all'];
      
      // Execute events
      var scopeArray = [];
      $.each(data.scope, function(i, scope){
        if (scope == 'all') {
          scopeArray.push('navigate');
          $.each($$.plugins, function(plugin) {
            scopeArray.push(plugin);
          });
        }
        else {
          scopeArray.push(scope);
        }        
      });
      $.each(scopeArray, function(i, scope){
        switch(scope) {
          case 'navigate':
            var eventArray = [];
            $.each(data.event, function(i, dataEvent){
              if (dataEvent == 'all') {
                $.each($$.events, function(i, event){
                  eventArray.push(event);
                });
              }
              else {
                eventArray.push(dataEvent);
              }
            });
            $.each(eventArray, function(i, event){
              if ($$.isFunction($$.events[event])){
                if (event == 'initialize' && $$.settings.loaded) return;
                $$.events[event](data);
                if (event == 'initialize') $$.settings.loaded = true;
              }
            });
            break;
          default:
            if ($$.pluginExists(scope)) {
              data.plugin = $$.plugins[scope];
              var eventArray = [];
              $.each(data.event, function(i, dataEvent){
                if (dataEvent == 'all') {
                  $.each($$.plugins[scope].events, function(i, event){
                    eventArray.push(event);
                  });
                }
                else {
                  eventArray.push(dataEvent);
                }
              });
              $.each(eventArray, function(i, event){
                if ($$.isFunction($$.plugins[scope].events[event])) {
                  if (event == 'initialize' && $$.plugins[scope].settings.loaded) return;
                  $$.plugins[scope].events[event](data);
                  if (event == 'initialize') $$.plugins[scope].settings.loaded = true;
                }
              });
            }
            break;
        }
      });
    },
    getData: function(element) {
      if (element == undefined) return false;
      var parent = $(element);
      var data;
      do {
        data = parent.data('navigate');
        if (data) {
          return data;
        }
        else {
          if (parent.is('#navigate') || parent.is('body')) {
            return false;
          }
          parent = parent.parent();
        }
      } while (true);
    },
    // Get the wid from the closest navigate html tag
    getWid: function(element) {
      if (element == undefined) return false;
      var data = $$.getData(element);
      if (data != undefined && data.wid != undefined) {
        return data.wid;
      }
      else {
        return false;
      }
    },
    /**
    * Find a widget or set of widgets.
    *
    * @param {number|string|object|array} data
    *   Data is processed in the following order to determine the widget(s) that it should return:
    *     - {number}
    *       - {wid}:      A number containing the widget ID 
    *     - {string}
    *       - {wid}:      A string containing the number of a widget ID.
    *       - {plugin}:   A string containing the plugin name. This will return all widgets that were created by a certain plugin type.
    *     - {object}
    *       - {element}:  A DOM Element, Document or jQuery element. This will return the widget the element is a child of.
    *     - {array}
    *       - {number|string|object}:   An array of the previously accepted types. This is not recursive, do not nest arrays.
    * @return
    *   jQuery object of the widget(s) or FALSE if no critera match.
    *
    * Example usage:
    * @code
    *   // Returns the widget: 1.
    *   $$.getWidget(1);                              
    *   // Same as above, but with quotes. Either works.
    *   $$.getWidget('1');            
    *   // Returns the widgets: 1, 4, 5 and 6                
    *   $$.getWidget([ 1, 4, 5, 6 ]);     
    *   // Returns all widgets of the navigate_menu plugin type                 
    *   $$.getWidget('navigate_menu');
    *   // Returns the widget that this callback button is a child of.
    *   $$.getWidget($('.some-callback-button'));   
    *   // Returns the widgets: 3, all widgets that are navigate_search and the widget that the callback button is a child of.
    *   $$.getWidget([ 3, 'navigate_search', $('.yet-another-callback') ])
    * @endcode
    */
    getWidget: function(data) {
      if (data == undefined) return false;
      var widgets = [];
      if (!$$.isArray(data)) {
        data = [data];
      }
      for (i in data) {
        if ($$.isString(data[i]) || $$.isNumber(data[i])) {
          if ($$.widgetExists(data[i])) {
            $('#navigate-widget-'+data[i]).each(function(){
              widgets.push(this);
            });
          }
          else if ($$.pluginExists(data[i])) {
            $('navigate[plugin=' + data[i] + ']', '.navigate-widget').parents('.navigate-widget').each(function(){
              widgets.push(this);
            });
          }
          else {
            var element = $$.closest('.navigate-widget', data[i]);
            if (element) {
              element.each(function(){
                widgets.push(this);
              });
            }
          }
        }
        else if ($$.isObject(data[i])) {
          var element = $$.closest('.navigate-widget', data[i]);
          if (element) {
            element.each(function(){
              widgets.push(this);
            });
          }
        }
      }
      if (widgets.length) {
        return $($.unique(widgets));
      }
      else {
        return false;
      }
    },
    grippie: function(element) {
      $(element).each(function() {
        if ($$.isProcessed(this)) return false;
        var element = $(this).addClass('textarea-processed'), staticOffset = null;
        // When wrapping the text area, work around an IE margin bug. See:
        // http://jaspan.com/ie-inherited-margin-bug-form-elements-and-haslayout
        $(this).wrap('<div class="resizable-textarea"><span></span></div>')
        .parent().append($('<div class="grippie"></div>').mousedown(startDrag));

        var grippie = $('div.grippie', $(this).parent())[0];
        grippie.style.marginRight = (grippie.offsetWidth - $(this)[0].offsetWidth) +'px';

        function startDrag(e) {
          staticOffset = element.height() - e.pageY;
          element.css('opacity', 0.25);
          $(document).mousemove(performDrag).mouseup(endDrag);
          return false;
        }

        function performDrag(e) {
          element.height(Math.max(32, staticOffset + e.pageY) + 'px');
          return false;
        }
        function endDrag(e) {
          $(document).unbind("mousemove", performDrag).unbind("mouseup", endDrag);
          element.css('opacity', 1);
        }
      });
    },
    // Hide the navigate bar
    hide: function(save) {
      if (save == undefined) save = true;
      $$.settings.visible = false;
      $("body").stop().animate({ marginLeft: "0px"}, { queue:true, duration:'fast' });
      $("#navigate").stop().removeClass('visible').animate({ left: '-' + ($$.settings.width + 25) + 'px' }, {
        queue:    false,
        duration: 'fast',
        complete: function() {
          if (save) {
            $$.variableSet('visible', false);
          }
        }
      });
    },
    isArray: function(obj) {
      if (!typeof obj == 'string') return false;
		  return Object.prototype.toString.call(obj) === '[object Array]';
  	},
  	isChecked: function(obj){
      if ($(obj).attr('type') === 'checkbox') {
        if ($(obj).is(':checked')) {
          return true;
        }
        else {
          return false;
        }
      }
      else {
        return null;
      }
    },
    isFunction: function(obj) {
      if (!typeof obj == 'string') return false;
		  return Object.prototype.toString.call(obj) === '[object Function]';
  	},
  	isNumber: function(obj) {
  	  if (typeof obj == 'number' || parseFloat(obj)==obj) {
        return true;
      }
      else {
        return false;
      }
    },    
    /**
     * Determine if an element has already been processed by an event
     *
     * @param element
     *   A DOM Element, Document or jQuery to use as context. (default: $('#navigate'))
     *
     * @return
     *   TRUE if the widget exists or FALSE if the widget does not exist.
     */
    isProcessed: function(element, process){
      var process = process || true;
      if ($(element).hasClass('processed')) {
        return true;
      }
      else {
        if (process) {
          $(this).addClass('processed');
        }
        return false
      }
    },
    isObject: function(obj) {
      if (!typeof obj == 'string') return false;
      return Object.prototype.toString.call(obj) === '[object Object]';
  	},
  	isString: function(obj) {
		  return typeof obj == 'string';
  	},
  	minimumVersion: function(minimum, current) {
      var minimum = '' + minimum;
      var current = '' + current;
      var parseVersion = function(version) {
        version = /(\d+)\.?(\d+)?\.?(\d+)?/.exec(version);
        return {
          major: parseInt(version[1]) || 0,
          minor: parseInt(version[2]) || 0,
          patch: parseInt(version[3]) || 0
        }
      };
      minimum = parseVersion(minimum);
      current = parseVersion(current);
      if (minimum.major != current.major)
        return (current.major > minimum.major);
      else {
        if (minimum.minor != current.minor)
          return (current.minor > minimum.minor);
        else {
          if (minimum.patch != current.patch) {
              return (current.patch > minimum.patch);
          }
          else {
            return true;
          }
        }
      } 
    },
  	parseFunction: function(data, defaults) {
  	  var regex = XRegExp("^function\\s*?\\((.*)?\\)\\s*?{(.*)}$", 'ims');
      data = regex.exec(data);
      if (!data) {
        if (defaults) return defaults;
        return false;
      }
      else {
        var arg = [];
        if (data[1]) {
          $.each(data[1].split(','), function(i,v){
            arg.push(v.replace(/^\s+|\s+$/g,""));
          });
        }
        else {
          if (defaults && defaults.arg) arg = $.extend(arg, defaults.arg);
        }
        return {
          arg: arg,
          code: data[2].replace(/^\n|\n$/g,"").replace(/^\s+|\s+$/g,"")
        };
      }
  	},
  	/**
     * Parse a JSON response.
     *
     * The result is either the JSON object, or an object with 'status' 0 and 'data' an error message.
     */
    parseJSON: function(data) {
      if ((data.substring(0, 1) != '{') && (data.substring(0, 1) != '[')) {
        return { status: 0, data: data.length ? data : 'Unspecified error' };
      }
      return eval('(' + data + ');');
    },
  	/**
     * Create a plugin in the Navigate namespace. This ensures that the plugin has default object data necessary to interact with Navigate.
     *
     * @param {object} plugin
     *   Object containing plugin data.
     *
     */
    pluginCreate: function(plugin) {
      var plugin =	$.extend(true, {}, {
        events: {
          initialize: function() {}
        },
        settings: {
          plugin:         '',
          pluginName:     '',
          pluginVersion:  '1.0',
          loaded: false
        }
      }, plugin);
      if (plugin.settings.plugin == undefined || plugin.settings.plugin == '') {
        alert('Corrupt plugin data! An unknown plugin has tried calling Navigate.pluginCreate() without providing proper plugin data. Please re-check your plugin for any mistakes!\n\nFor documentation, visit:\nhttp://drupal.org/project/navigate');
        return;
      }
      if ($$.pluginExists(plugin.settings.plugin)) {
        $.extend(true, $$.plugins[plugin.settings.plugin], plugin);
      }
      else {
        $$.plugins[plugin.settings.plugin] = plugin
      }
      $$.execute({ event: 'pluginCreate' });
    },
    // Check to see if plugin exists
    pluginExists: function(name){
      return $$.plugins[name] != undefined ? true : false;
    },
    pluginjQuery: function(plugins) {
      var addScript = function(name) {
        $.ajax({
          url: $$.settings.jqueryPlugins + '/jquery.' + name + '.js',
          dataType: 'script',
          success: success
        });

      };
      var recursive = function(name, property, depth) {
        depth = depth || '';
        if ($$.isObject(property)) {
          var dom = (depth != '' ? depth + '.' + name : name);
          if ($$.isString(property.version) || $$.isNumber(property.version)) {
            if ($$.isString(eval(dom).version) || $$.isNumber(eval(dom).version)) {
              if (!$$.minimumVersion(property.version, eval(dom).version)) {

              }
            }
          }
          else {
            $.each(property, function(key, val, dom) {
              recursive(key, val)
            });
          }
        }
      }
      $.each(obj, function(name, property){
        recursive(name, property, name)
      });


      $.each(obj, function(name, property){
        var type = '';
        if ($$.isFunction(property)) type = 'function';
        if ($$.isObject(property)) type = 'object';
        switch (type) {
          case 'function':
            if (!$$.isFunction(window[name])) {

            }
            break;
          case 'object':
            break;
        }

        if (!$$.isFunction($[plugin])) {
          $.getScript($$.settings.jqueryPlugins + '/jquery.' + plugin + '.js');
        }
      });
    },
    // Add count to queue
    queueAdd: function() {
      if ($$.settings.queue < 1) {
        $("#navigate-queue").animate({ opacity: 1 }, { queue:false, duration:400 });
      }
      $$.settings.queue++;
    },
    // Remove count from queue
    queueSubtract: function() {
      $$.settings.queue--;
      if ($$.settings.queue < 1) {
        $("#navigate-queue").animate({ opacity: 0 }, { queue:false, duration:400 });
      }
    },
    // Reload navigate widget set
    reloadWidgets: function() {
      $('.navigate-widget-list').animate({'opacity': 0.4});
      $$.ajax({
        data: {
          'op':   'widgets_reload'
        },
        success: function(wid, msg) {
         $('.navigate-widget-list').html(msg);
         $('.navigate-widget-list').animate({'opacity': 1.0});
         $('.navigate-widget-delete').show();
         Drupal.attachBehaviors('.navigate-widget-list');
        }
     });
    },
    removeWidget: function(wid) {
      var widget = $$.getWidget(wid);
      if (widget.length) {
        $$.ajax({
          data: {
            wid:     wid,
            op:  'widget_delete'
          },
          success: function(wid, data) {
            widget.addClass('ui-sortable-remove');
            $$.widgets.length--;
          }
        });
      }
    },
    resizeSettings: function() {
      var viewportHeight = $(window).height();
      var viewportWidth = $(window).width() - $$.settings.width;
      var dialogHeight = viewportHeight / 1.2;
      var dialogWidth = viewportWidth / 1.2;
      var dialogPosX = $$.settings.width + (viewportWidth - dialogWidth) / 2;
      var dialogPosY = (viewportHeight - dialogHeight) / 2;
      $('#navigate-settings').dialog('option', { width: dialogWidth, height: dialogHeight, position: [dialogPosX, dialogPosY] });
    },
    setData: function() {
      // Store Navigate data in jQuery to improve performance
      $('input.navigate-data').each(function(){
        var input = $(this);
        input.parent().data('navigate', $$.parseJSON(input.val()));
        input.remove();
      });
    },
    // Create export content and populate textarea
    setExport: function() {
      $('.navigate-import-export').animate({'opacity': 0.4});
      $$.ajax({
        data: {
          op: 'export_widget_set'
        },
        success: function(wid, msg) {
          $('#navigate-favorites-export_').val(msg);
          $('.navigate-import-export').animate({'opacity': 1.0});
          Drupal.attachBehaviors();
        }
      });
    },
    // Create export content and populate textarea
    setImport: function() {
      $('.navigate-import-export').animate({'opacity': 0.4});
      $$.ajax({
        data: {
          'op':   'import_widget_set',
          'import':   $('#navigate-favorites-import_').val()
        },
        success: function(wid, msg) {
          $('#navigate-favorites-import_').val('');
          $('.navigate-import-export').animate({'opacity': 1.0});
          $$.reloadWidgets();
          Drupal.attachBehaviors();
          $$.slideWidget();
        }
      });
    },
    // Show the navigate bar
    show: function (save) {
      if (save == undefined) save = true;
      $$.settings.visible = true;
      $("body").stop().animate({ marginLeft: ($$.settings.width - 10) + "px"}, { queue:false, duration:'fast' } );
      $("#navigate").stop().addClass('visible').animate({ left: "0" }, {
        queue:    false,
        duration: 'fast',
        complete: function(){
          if (save) {
            $$.variableSet('visible', true);
          }
        }
      });
    },
    slideWidget: function(wid, pwid, callback) {
      if (!$$.settings.slider.previousWidget.animated && !$$.settings.slider.sorting) {
        // Set currentWidget wid
        if (wid == undefined)
          wid = $$.settings.slider.currentWidget.wid;
        if (!$$.widgetExists(wid))
          wid = $.cookie('Navigate.settings.slider.currentWidget.wid');
        if (!$$.widgetExists(wid))
          wid = $$.getWid($('.navigate-widget')[0]);
        if ($$.widgetExists(wid)) {
          $.cookie('Navigate.settings.slider.currentWidget.wid', wid, { path: '/' });
          $$.settings.slider.currentWidget.wid = wid;
        }
        else {
          return;
        }
  
        // Set previousWidget wid
        if (pwid == undefined)
          pwid = $$.settings.slider.previousWidget.wid;
        if (!$$.widgetExists(pwid))
          // pwid = wid;
          pwid = $$.getWid($('.navigate-widget').find('.navigate-widget-slider:hidden')[0]);
        if (wid != pwid && $$.widgetExists(pwid)) {
          $$.settings.slider.previousWidget.wid = pwid;
        }
  
        var contentHeight = $('.navigate-widget-list').height();

        var widget = $$.getWidget(wid);
        var widgetSlider = $('.navigate-widget-slider', widget);
        var widgetContent = $('.navigate-widget-content', widget);
        var widgetSettings = $('.navigate-widget-settings', widget);
        
        var closedWidgetHeights = $$.settings.slider.closedHeight * $$.widgets.length;
        var currentWidgetHeight = contentHeight - closedWidgetHeights;
        
        if ($$.settings.slider.autoHeight) {
          if ($$.widgets[wid].settingsVisible) {
            currentWidgetHeight = (widgetSettings.outerHeight(true) < currentWidgetHeight ? widgetSettings.outerHeight(true) : currentWidgetHeight );
          }
          else {
            currentWidgetHeight = (widgetContent.outerHeight(true) < currentWidgetHeight ? widgetContent.outerHeight(true) : currentWidgetHeight );
          }
        }
        
        if (widgetContent.height() > widget.height()) {
          widgetContent.bind('mousewheel', function(event, delta) {
            var currentTop = parseInt($(this).css('position', 'relative').css('top'));
            var maxTop = $(this).parent().height() - $(this).height();
            var newTop = currentTop + (delta * $$.settings.mouseScrollSpeed);
            if (newTop > 0 ) newTop = 0;
            if (newTop <= maxTop) newTop = maxTop;
            if (currentTop != newTop) {
              $(this).stop().animate({ top: newTop + 'px' }, {queue: false, duration: 1, easing: 'swing'});
            }
            return false;
          });
        }
        else {
          widgetContent.unbind('mousewheel');
        }
        
        // if (currentWidgetSettingsHeight > currentWidgetHeight) {
        //   currentWidgetSettings.bind('mousewheel', function(event, delta) {
        //     var currentTop = parseInt($(this).css('position', 'relative').css('top'));
        //     var maxTop = $(this).parent().height() - $(this).height();
        //     var newTop = currentTop + (delta * $$.settings.mouseScrollSpeed);
        //     if (newTop > 0 ) newTop = 0;
        //     if (newTop <= maxTop) newTop = maxTop;
        //     if (currentTop != newTop) {
        //       $(this).css({ top: newTop + 'px' });
        //     }
        //     return false;
        //   });
        // }
        // else {
        //   currentWidgetContent.unbind('mousewheel');
        // }

        if (wid != pwid && $$.widgetExists(pwid)) {
          $$.settings.slider.previousWidget.animated = true;
          $('#navigate-widget-'+pwid).find('.navigate-widget-slider').stop().animate({ height: 0, opacity: 0 }, { queue: false, duration: 400, complete: function() {
            $$.settings.slider.previousWidget.animated = false;
          }}).css('visibility', 'hidden');
        }
        widgetSlider.css('visibility', 'visible');
        widgetContent.css('visibility', 'visible');
  	    widgetSlider.stop().animate({ height: currentWidgetHeight+"px", opacity: 1 }, { queue:false, duration: 400 });
    	  if ($$.isFunction(callback)) {
          callback(wid);
        }
      }
    },
    sortWidgets: function(element, context, method) {
      var element = element;
      var context = context || $('#navigate');
      if ($(element).length) {
        if (method) {
          $(element, context).sortable(method);
        }
        else {
          $(element, context).sortable({
            cursor: "move",
            connectWith: element,
            distance: 10,
            handle: '.navigate-widget-title',
            zIndex: 5100,
            // forcePlaceholderSize: true,
            start: function(e, ui) {
              $$.settings.slider.sorting = true;
              ui.item.stop().animate({ height: 1 }, { queue: false, duration: 400 });
              ui.item.find('.navigate-widget-slider').stop().animate({ opacity: 0 }, { queue: false, duration: 400 });
              ui.placeholder.stop().animate({ height: $$.settings.slider.closedHeight - 20 }, { queue: false, duration: 400 });                
              // $(ui.item).stop().animate({ shadow: '10px 10px 15px 0 rgba(0,0,0,0.65)'}, { queue: true, duration: 400 })
            },
            stop:function(e, ui) {
              var allWidgets = $(".navigate-widget-list");
              $('.ui-sortable-remove', allWidgets).each(function(){
                $(this).remove();
              });
              var widgets = allWidgets.sortable('toArray');
              var widgetClass = '';
              var widgetOrder = [];
              for(x in widgets) {
                if (widgetClass == '') {
                  widgetClass = widgets[x].match(/\w+-\w+/).toString()+'[]';
                }
                widgetOrder.push(widgets[x].match(/\d+/).toString());
              }
              var processedData = {
                op: 'widget_sort'
              };
              processedData[widgetClass] = widgetOrder;
              $$.ajax({
                data: processedData,
                success: function() {
                  $(ui.item).stop().animate({ shadow: '0 0 15px 0 rgba(9,23,47,1)'}, { queue: true, duration: 400 })
                  $$.settings.slider.sorting = false;
                  $$.slideWidget();
                }
              });
            }
          });
        }
      }
    },
    // Hide / show navigate bar
	  toggle: function() {
      if ($$.settings.visible) {
       $$.hide();
      }
      else {
        $$.show();
      }
    },
    toggleWidgetSettings: function(wid) {
      var widget = $$.getWidget(wid);
      var widgetShadowOverlay = '0 0 15px 0 rgba(255,255,255,0.5)';
      var overlay = $('#navigate-overlay');
      var settings = $('.navigate-widget-settings', widget);
      var content = $('.navigate-widget-content', widget);
      var boxShadowType;
      // Determine which shadow type should be used
      $.each(['boxShadow', 'MozBoxShadow', 'WebkitBoxShadow'], function(i, property) {
    		var val = $('body').css(property);
    		if (typeof val == 'string' && val != '') {
    			boxShadowType = property;
    			return false;
    		}
    	});      
      if (settings.length) {
        if (settings.is(':hidden')) {
          widget.data('shadow', widget.css(boxShadowType));
          widget.css('z-index', 5101).animate({ shadow: widgetShadowOverlay }, { queue: true, duration: 400 });
          overlay.fadeIn(400);
          content.fadeOut(400, function(){
            settings.fadeIn();
          });
          $$.widgets[wid].settingsVisible = true;
        }
        else {
          widget.animate({ shadow: widget.data('shadow') }, { queue: true, duration: 400 });
          overlay.fadeOut(400, function() {
            widget.css('z-index', 5000);
          });
          settings.fadeOut(400, function(){
            content.fadeIn();
          });
          $$.widgets[wid].settingsVisible = false;
        }
        $$.slideWidget(wid, $$.settings.slider.currentWidget.wid);
      }
    },
    urlEncode: function(value){
      // Convert booleans
      if (value === true || value === false) {
       value = '(bool)' + (value === true ? '1' : '0');
      }
      value = encodeURIComponent(value);
      return value;
    },
    // Set a variable
    variableSet: function(name, value, wid) {
      // Save the variable in a database
      $$.ajax({
        data: {
          op:   'variable_set',
          name:     name,
          value:    value,
          wid:      wid
        }
      });
    },
    /**
     * Determine if a widget exists
     *
     * @param wid
     *   An integer containing the ID of the desired widget.
     *
     * @return
     *   TRUE if the widget exists or FALSE if the widget does not exist.
     */
    widgetExists: function(wid) {
      return $('#navigate-widget-'+wid).length ? true : false;
    },
    

//  *******************************************************
//  Navigate Events
    events: {
      
      // Binds click event to list of widgets
      bindSettings: function(data) {
        var behaviorData = data;
        
        // Add widget plugin list
        $('.navigate-plugin', $('#navigate-plugin-list')).each(function(){
          if ($$.isProcessed(this)) return;
          $(this).click(function() {
            var item = $$.getData(this);
            // If it's a one-at-a-time widget, return false
            if (item.single == '1') {
              if ($('.navigate-widget > .type[value="' + $(this).attr('id') + '"]').length > 0) {
                alert(t('Sorry, only one instance of this widget is allowed at one time.'));
                return false;
              }
            }
            $$.ajax({
              data: {
                op:   'widget_add',
                plugin:   item.plugin,
              },
              success: function(wid, data) {
                $('.navigate-widget-list').append(data.content);
                var widget = $('#navigate-widget-'+data.wid);
                $$.widgets.length++;
                if (item.callback != '' && item.callback != undefined) {
                  if ($$.isFunction($$.plugins[item.pluginName][item.callback])) {
                    $$.plugins[item.pluginName][item.callback](widget);
                  }
                }
                $$.toggleWidgetSettings(data.wid);
                $$.execute({ event: 'bindWidget' }, behaviorData);
              }
            });
            return true;
          });
        });
      },
      // Bind textarea save event, including value saving and callback on pressing enter
      bindAdminTools: function(data) {
        if ($('.navigate-admin-tools-defaults').hasClass('navigate-admin-tools-defaults-processed')) {
          return;
        }
        $('.navigate-admin-tools-defaults').addClass('navigate-admin-tools-defaults-processed');

        // Set default
        $('.navigate-default-set').click(function () {
          $(this).addClass('navigate-default-set-processed');
          // Save the variable in a database
          $('.navigate-defaults').animate({'opacity': 0.4});
          $$.ajax({
            data:    {
              op:  'save_defaults',
              rid:     $(this).attr('rel')
            },
            success: function(wid, msg) {
              $('.navigate-defaults').html(msg);
              $('.navigate-defaults').animate({'opacity': 1.0});
              Drupal.attachBehaviors('.navigate-defaults');
            }
          });
          return false;
        });

        // Unset default
        $('.navigate-default-unset').click(function () {
          // Save the variable in a database
          $('.navigate-defaults').animate({'opacity': 0.4});
          $$.ajax({
            data: {
              op:   'defaults_unset',
              rid:      $(this).attr('rel')
            },
            success: function(wid, msg) {
              $('.navigate-defaults').html(msg);
              $('.navigate-defaults').animate({'opacity': 1.0});
              Drupal.attachBehaviors('.navigate-defaults');
            }
          });
          return false;
        });

        // Load default
        $('.navigate-default-load').click(function () {
          // Save the variable in a database
          $$.queueAdd();
          $('.navigate-defaults').animate({'opacity': 0.4});
          $$.ajax({
            data: {
              op: 'defaults_load',
              rid:    $(this).attr('rel'),
            },
            success: function(msg) {
              $('.navigate-defaults').html(msg);
              $$.reloadWidgets();
              $('.navigate-defaults').animate({'opacity': 1.0});
              Drupal.attachBehaviors('.navigate-defaults');
            }
          });
          return false;
        });

        // Switch back to saved set
        $('.navigate-default-switch-back').click(function () {
          // Save the variable in a database
          $('.navigate-defaults').animate({'opacity': 0.4});
          $$.ajax({
            data: {
              op: 'defaults_switch_back'
            },
            success: function(msg) {
              $('.navigate-defaults').html(msg);
              $$.reloadWidgets();
              $('.navigate-defaults').animate({'opacity': 1.0});
              Drupal.attachBehaviors('.navigate-defaults');
            }
          });
          return false;
        });

        // Switch back to saved set
        $('.navigate-default-set-all').click(function () {
          if (!confirm('Doing this will erase the current custom set for all users in this role, and will set this set as the default for this role. Are you sure you want to continue?')) {
            return false;
          }
          // Save the variable in a database
          $$.queueAdd();
          $('.navigate-defaults').animate({'opacity': 0.4});
          $.ajax({
             url: $$.ajaxUrl(),
             type: 'POST',
             data: 'op=defaults_set_all&rid=' + $(this).attr('rel'),
             error: function() {
             },
             success: function(msg) {
              $('.navigate-defaults').html(msg);
              $$.reloadWidgets();
              $('.navigate-defaults').animate({'opacity': 1.0});
              $$.queueSubtract();
              Drupal.attachBehaviors('.navigate-defaults');
             }
          });
          return false;
        });

        // Bind user search check
        $('#navigate-set-username').bind('keyup',function () {
          if ($(this).val() != '') {
            $('.navigate-default-set-user-load').html('<a class="navigate-default-load-user" href="javascript:;">' + Drupal.t('Load') + '</a>');
            $('.navigate-default-set-user-set').html('<a class="navigate-default-set-user" href="javascript:;">' + Drupal.t('Set user') + '</a>');
          } else {
            $('.navigate-default-set-user-load').html('<span class="default-disabled">' + Drupal.t('Load') + '</span>');
            $('.navigate-default-set-user-set').html('<span class="default-disabled">' + Drupal.t('Set user') + '</span>');
          }
          Drupal.attachBehaviors('.navigate-defaults-table');
        });

        // Add instructions to empty username search
        $('#navigate-set-username').focus(function () {
          $(this).removeClass('navigate-username-empty');
          if ($(this).val() == Drupal.t('Search name / UID')) {
            $(this).val('');
          }
        });

        // Add instructions to empty username search
        $('#navigate-set-username').blur(function () {
          if ($(this).val() == '') {
            $(this).addClass('navigate-username-empty');
            $(this).val(Drupal.t('Search name / UID'));
          }
        });
      },
      // Bind callback click actions to buttons.
      bindCallbackButtons: function(data) {
        $('.navigate-button').each(function() {
          if ($$.isProcessed(this)) return;
          $(this).click(function() {
            var button = $$.getData(this);
            if (button.wid == '' || button.wid == undefined) {
              button.wid = $$.getWid(this);
            }
            if (button.callback != '' && button.callback != undefined) {
              if (button.plugin != '' && button.plugin != undefined) {
                $$.plugins[button.plugin][button.callback](button.wid);
              }
              else {
                Navigate[button.callback](button.wid);
              }
            }
          });
        });
      },
      // Binds events and various errata after page is loaded
      initialize: function(data) {
        
        $$.setData();
                
        // Make Navigate resizeable
        $("#navigate").resizable({
    			maxWidth: 500,
    			minWidth: 200,
    			handles: 'e',
    			resize: function(event, ui) {
    			  $('body').css( 'margin-left', ui.size.width - 10);
    			  $('#navigate-widget-remove').css('left', ui.size.width);
    			},
    			stop: function(event, ui) {
    			  $(this).css('height', '');
    			  $$.settings.width = ui.size.width;
    			}
    		});
    		
    		$$.settings.width = $('#navigate').outerWidth(true);
			  $('#navigate-widget-remove').css('left', $$.settings.width);
			  
			  // Set initial styling.
        if ($$.settings.visible) {
          $$.show(false);
        }
        else {
          $$.hide(false);
        }
        $("#navigate-queue").css({ opacity: 0 });
        
        // Remove widget droppable
        $("#navigate-widget-remove").droppable({
        	accept: '.navigate-widget',
    			activeClass: 'ui-state-hover',
    			hoverClass: 'ui-state-active',
          tolerance: 'pointer',
    			over: function(e, ui) {
    			  ui.draggable.addClass('remove-widget');
    			},
    			out: function(e, ui) {
    			  ui.draggable.removeClass('remove-widget');    			  
    			},
    			drop: function(e, ui) {
    			  ui.draggable.fadeOut(400);
    			  $$.removeWidget($$.getWid(ui.draggable));
    			}
    		});
    		
        $$.execute({ event: [
          'bindAdminTools',
          'bindSettings',
          'bindWidget'
        ]}, data);
      
        // Run one-time bindings on Navigate area
        $('#navigate-settings').tabs({
          idPrefix: 'navigate-settings-',
          select: function(event, ui) {
            var jsCallback = $$.getData(ui.tab).jsCallback;
            var pageCallback = $$.getData(ui.tab).pageCallback;
            if (pageCallback != undefined) {
              $$.ajax({
                data: pageCallback,
                success: function(json) {
                  $(ui.panel).html(json.content);
                  if (jsCallback != undefined) {
                    eval(jsCallback);
                  }
                }
              });
            }
          },
          show: function(event, ui) {
            $(ui.panel).jScrollPane();
          }
        }).dialog({
          autoOpen: false,
          closeText: 'x',
          dialogClass: 'navigate-dialog',
          draggable: false,
          minWidth: 500,
          minHeight: 300,
          modal: true,
          position: 'right',
          resizable: false,
          title: 'Navigate Settings',
          width: 700,
          zIndex: 4800,
          open: function(event, ui) {
            $('.ui-dialog-overlay').css('z-index', 4700);
          }
        });
        
        $('#navigate-settings-button').click(function(){
          $$.resizeSettings();
          $('#navigate-settings').dialog('open');
        });

         // Bind click event to setting button to show close buttons and new widgets list
        // $('#navigate-settings').click(function() {
        //   $(".navigate-admin-tools-outer").slideToggle('fast', function() {
        //     $(this).animate({'opacity': 0.9});
        //     $$.slideWidget();
        //   });
        //   $('.navigate-add-widgets').slideToggle('fast', function() {
        //     $('.navigate-widget-delete').toggle();
        //   });
        // });

        // Add select all on text inputs with select all specified
        $(".navigate-select-all").focus(function () {
          $(this).select();
        });

        $("#navigate-switch").click($$.toggle);


        // Bind click event to acklowledgement link
        $(".navigate-acknowledgement-switch").click(function () {
          $('.navigate-acknowledgement').slideToggle('fast');
        });

        // Bind doubleclick event to title
        $('.navigate-title').dblclick(function() {
          $('.navigate-shorten').slideToggle('fast');
        });

        // Bind help button
        $('.navigate-help-button').click(function () { window.location = $(this).find('.value').val();});

        // Bind focus and blur text inputs to show/hide box shadow
        $('.navigate-text-input').each(function(){
          $(this).focus(function(){
            $(this).addClass('focus');
          });
          $(this).blur(function(){
            $(this).removeClass('focus');
          });
        });

        // Bind keys
        $(document).bind('keydown', {combi:'ctrl+shift+n'}, $$.toggle);
        if ($('.navigate-key-button').hasClass('key-disabled')) {
          $(document).unbind('keydown', {combi:'ctrl+shift+n'});
        }

        // Add select all on text inputs
        $('.navigate-select-all').each(function () {
          if ($(this).hasClass('select-all-processed')) {
            return;
          }
          $(this).addClass('select-all-processed');
          $(".navigate-select-all").focus(function () {
            $(this).select();
          });
        });


        // Add tooltips - v.2
        $('.navigate-tooltip').each(function () {
          if ($(this).hasClass('tooltip-processed')) {
            return;
          }
          $(this).addClass('tooltip-processed');
          $content = $('#' + $(this).attr('id') + '_tip_content').html();
          if ($content != '') {
            $(this).tooltip({
              bodyHandler: function() {
                return $('#' + $(this).attr('id') + '_tip_content').html();
              },
              track: true,
              fixPNG: true,
              fade: 150,
              delay:750
            });
          }
        });

        // Bind actions to 'Load' for single users load / set options
        $('.navigate-default-load-user:not(.navigate-default-load-user-processed)').each(function () {
          $(this).addClass('navigate-default-load-user-processed');
          $(this).click(function () {
            $$.queueAdd();
            $.ajax({
              url: $$.ajaxUrl(),
              type: 'POST',
              data: 'op=defaults_load&username=' + $('#navigate-set-username').val(),
              error: function() {
              },
              success: function(msg) {
                $('.navigate-defaults').html(msg);
                $$.reloadWidgets();
                $('.navigate-defaults').animate({'opacity': 1.0});
                $$.queueSubtract();
                Drupal.attachBehaviors('.navigate-defaults');
              }
            });
          });
        });


        // Bind actions to 'Load' for single users load / set options
        $('.navigate-default-set-user:not(.navigate-default-set-user-processed)').each(function () {
          $(this).addClass('navigate-default-set-user-processed');
          $(this).click(function () {
            if (!confirm('Doing this will erase the current custom set for this user. Are you sure you want to continue?')) {
              return false;
            }
            $$.queueAdd();
            $.ajax({
              url: $$.ajaxUrl(),
              type: 'POST',
              data: 'op=defaults_set_all&username=' + $('#navigate-set-username').val(),
              error: function() {
              },
              success: function(msg) {
                $('.navigate-defaults').html(msg);
                $$.reloadWidgets();
                $('.navigate-defaults').animate({'opacity': 1.0});
                $$.queueSubtract();
                Drupal.attachBehaviors('.navigate-defaults');
              }
            });
          });
        });

        // Bind keyboard shortcut enable / disable
        $('.navigate-key-button:not(.navigate-key-button-processed)').each(function () {
          $(this).addClass('navigate-key-button-processed');

          $(this).click(function () {
            if ($(this).hasClass('key-disabled')) {
              $(document).bind('keydown', {combi:'ctrl+shift+n'});
              $(this).removeClass('key-disabled');
              $$.variableSet('keys', false);
            } else {
              $(document).unbind('keydown', {combi:'ctrl+shift+n'});
              $(this).addClass('key-disabled');
              $$.variableSet('keys', true);
            }
          });
        });

        // Bind import / export button
        $('.navigate-import-button:not(.navigate-import-button-procesed)').each(function () {
          $(this).click(function () {

          });
        });

        // Execute windowResize
        $(window).resize(function() {
          $$.execute({ event: ['windowResize']}, data);
        });
      },
      // Bind textarea save event, including value saving and callback on pressing enter
      bindTextAreas: function(data) {
        $('.navigate-textarea-outer').each(function() {
          if ($$.isProcessed(this)) return;
          var item = $$.getData(this);
          $(this).find('.navigate-submit').click(function() {
            var val = $(widget).find('.navigate-textarea').val();
            $$.variableSet(item.name, val, item.wid);
            $(widget).find('.navigate-textarea').html(val);
            if (item.callback != '' && item.callback != undefined) {
              Navigate[item.callback](item.wid);
            }
          });

        });
      },
      // Bind text input events, including value saving and callback on pressing enter
      bindTextInputs: function(data) {
        $('.navigate-text-input-outer').each(function() {
          if ($$.isProcessed(this)) return;
          var item = $$.getData(this);
          var text_input = this;
          $(this).find('input').bind('keyup', function(e) {
            var input_name = $(text_input).children('div').children('input').attr('name');
            var id = $(text_input).children('div').children('input').attr('id');
            var holder_id = item.name + item.wid + '_holder';
            // If pressing enter
            if (e.which == 13 || e.keyCode == 13) {
              if (item.callback != '' && item.callback != undefined) {
                if (item.plugin != '' && item.plugin != undefined) {
                  $$.plugins[item.widgetType][item.callback](item.wid);
                }
                else {
                  Navigate[item.callback](item.wid);
                }
                if ($(this).hasClass('navigate-clear')) {
                  var val = '';
                  $(this).val('');
                }
              }
            }
          });

        });
      },
      // Bind click actions to on/off buttons.
      bindToggleButtons: function(data) {
        $('.navigate-button-outer').each(function() {
          if (!$(this).hasClass('toggle-button-processed')) {
            $(this).addClass('toggle-button-processed');
            var wid = $$.getWid(this);
            var widget = this;
            $(this).children().click(function() {

              // Grab all variables from hidden inputs
              var on = $(widget).find('.on').val();
              var off = $(widget).find('.off').val();
              var name = $(widget).find('.name').val();
              var required = $(widget).find('.required').val();
              var callback = $(widget).find('.callback').val();
              var val;

              // If it's a group
              if ($(widget).find('.group').length > 0) {
                var group = $(widget).find('.group').val();
                if ($(this).hasClass('navigate-button-on')) {
                  if (required == 1) {
                    return;
                  }
                  $(this).parents('.navigate-widget').find('.' + group).each(function(child) {
                    $(this).removeClass('navigate-button-on');
                  });
                  $(this).removeClass('navigate-button-on');
                  $$.variableSet(name, '', wid);
                  return;
                }
                $(this).parents('.navigate-widget').find('.' + group).each(function(child) {
                  $(this).removeClass('navigate-button-on');
                });
                val = on;
                $(this).addClass('navigate-button-on');

              // If not a group
              } else {
                // If it's just a callback
                if (on != '') {
                  if ($(this).hasClass('navigate-button-on')) {
                    val = off;
                    $(this).removeClass('navigate-button-on');
                  } else {
                    val = on;
                    $(this).addClass('navigate-button-on');
                  }
                }
              }
              if (callback != '' && callback != undefined) {
                Navigate[callback](wid);
              }
              $$.variableSet(name, val, wid);
            });
          }
        });
      },
      // Bind widget with standard behaviors
      bindWidget: function(data) {
        $$.setData();

        // Add widget(s) to Navigate namespace
        var widgetCount = 0;
        $('.navigate-widget', data.context).each(function(){
          var wid = $$.getWid(this);
          $$.widgets[wid] = new Object;
          $$.widgets[wid] = $(this);
          $$.widgets[wid].settingsVisible = false;
          widgetCount++
        });
        $$.widgets.length = widgetCount;
        
        // Bind widget title
        $(".navigate-widget-title", data.context).each(function(index){
          // Make widgets slide like an accordion
          $(this).hoverIntent(function(){
            $$.slideWidget($$.getWid(this), $$.settings.slider.currentWidget.wid);
          }, function(){});
          // Bind double click to customize widget title
          if ($(this).hasClass('customize')) {
            $(this).dblclick(function(){
              $$.customizeTitle(this);
            });
          }
          else {
            $(this).unbind('dblclick');
          }
        });
        
        // Bind widget settings
        $('.navigate-widget-settings-button', data.context).each(function() {
          // Skip if already processed
          if ($$.isProcessed(this)) return;          
          $(this).click(function() {
            $$.toggleWidgetSettings($$.getWid(this));
          });
        });
        $('.navigate-widget-settings', data.context).each(function(){
          // Reset widget
          $('.navigate-widget-settings-close', this).click(function(){
            $$.toggleWidgetSettings($$.getWid(this));
          });
        });
        
        // Bind widget delete button
        $('.navigate-widget-delete', data.context).click(function() {
          if ($$.isProcessed(this)) return;
          var wid = $$.getWid(this);
          $$.removeWidget(wid);
        });
        
         // Make widgets sortable using their title as the handle
        $$.sortWidgets('.navigate-widget-list.customize', data.context);
        
        // $('.naviagte-widget').draggable()
        
        // Attach additional behaviors
        $$.execute({ event: [
          'bindCallbackButtons',
          'bindTextAreas',
          'bindTextInputs',
          'bindToggleButtons'
        ]}, data);
        $$.slideWidget();
      },
      windowResize: function(data) {
        $$.slideWidget();
        $$.resizeSettings();
      }
    }
  });

  // Extend Drupal into Navigate's namespace then remove it
  $.extend(true, $$, Drupal.settings.Navigate);
  delete Drupal.settings.Navigate;

  // Intiate Navigate and Plugin initialize
  $(document).ready(function(){
    if (Navigate.jQuery) {
      $$.execute({ event: 'initialize' });
    }
  });

})(Navigate.jQuery, Navigate);

