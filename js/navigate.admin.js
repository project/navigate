;(function($, $$) {   // Reference: $ => Navigate.jQuery and $$ => Navigate
  $$.pluginCreate({
    // Plugin Settings    
    settings: {
      plugin:         'navigate_admin',
      pluginName:     'Navigate Admin',
      pluginVersion:  '2.0-beta1',
      permissionState: 'all'
    },
    // Events
    events: {
      initialize: function(data) {
        plugin = data.plugin;
        
        $('form').each(function(){
          var form = $(this);
          var formName = form.attr('id');
          switch(formName) {
            case 'navigate-admin-settings':
              // Setup render options
              var renderRadios = $('input[name="render"]', form);
              function checkRenderRadios() {
                var render = renderRadios.filter(':checked').val();
                var ajaxDiv = $('#edit-render-ajax-wrapper');
                switch (render) {
                  case 'slider':
                    ajaxDiv.slideDown();
                    break;
                  case 'stacked':
                    ajaxDiv.slideUp();
                    break;
                }
              }
              renderRadios.change(function(){
                checkRenderRadios();
              });
              checkRenderRadios();
              break;
            case 'navigate-permission-rule':
              break;
          }
        });
        
        $('a.ids').click(function(){
          var link = $(this);
          switch(link.text()) {
            case 'show':
              link.text('hide');
              link.next().slideDown('fast');
              break;
            case 'hide':
              link.text('show');
              link.next().slideUp('fast');
              break;
          }
          return false;
        });
        
        // http://www.ryancramer.com/journal/entries/radio_buttons_firefox/
        // Fix for Firefox's "RRR (Radio Roaming Refresh)"
        // $('form').attr('autocomplete', 'off');
        
        // uid and rid autocomplete
	      var ids = $('#edit-ids'), errorClass = '';
	      if (ids.length) {
  	      if (ids.hasClass('error')) {
  	        errorClass = ' error';
  	      }
  	      ids.addClass('textarea-processed').parents('div.form-item').css('position', 'relative');
      		ids.tokenInput(Drupal.settings.basePath + 'ajax/navigate/autocomplete', {
      		  categories: true,
            contentType: "json",
      		  classes: {
      		      fieldset: "token-input-navigate" + errorClass,
                tokenList: "token-input-list-navigate",
                token: "token-input-token-navigate",
                tokenDelete: "token-input-delete-token-navigate",
                selectedToken: "token-input-selected-token-navigate",
                highlightedToken: "token-input-highlighted-token-navigate",
                dropdown: "token-input-dropdown-navigate",
                dropdownCategory: "token-input-dropdown-category-navigate",
                dropdownItem: "token-input-dropdown-item-navigate",
                dropdownItemZebra: "token-input-dropdown-item-navigate odd",
                dropdownItemDescription: "token-input-dropdown-item-description-navigate",
                selectedDropdownItem: "token-input-selected-dropdown-item-navigate",
                inputToken: "token-input-input-token-navigate"
            },
            descriptions: true,
            dropdownAnimation: 'show',
            dropdownDuration: 0,
            dropdownDuplicates: false,
            duplicates: false,
            existingParam: 'existing',
            hintText: "Type in a username, uid, role or rid",
            noResultsText: "No users or roles found",
            searchingText: "Searching...",
            method: "POST",
            minChars: 0,
            onResult: null,
            prePopulate: function() {
              var json = [];
              $$.ajax({
                async: false,
                data: {
                  op: 'prepopulate',
                  prepopulate: ids.val().replace(/,$/, '')
                },
                success: function(data) {
                  json = data;
                },
                url: Drupal.settings.basePath + 'ajax/navigate/autocomplete'
              });
              return json;
            },
            tokenDuplicates: false,
            queryParam: "term"
      		});
  		  }
    		        
        // Permissions
        var permissionsType = $('input[name="type"]');
        if (permissionsType.length) {
          var tbody = $$.closest('tbody', permissionsType);
          var permissions = $('input[name^="permissions"]', tbody);
          permissions.each(function(){
            $(this).data('check-state', $$.isChecked(this));
            $(this).change(function(){
              if (plugin.settings.permissionState == 'custom') {
                plugin.checkDependents(this);
              }
            });
          });
          // Permission Type
          $(permissionsType).change(function(){
            plugin.permissionsType(permissionsType);
          });
          plugin.permissionsType(permissionsType);
          // Permission dependency
          plugin.processDependents(tbody); 
        }
      }
    },
    checkDependents: function(checkbox) {
      var row = $$.closest('tr', checkbox);
      var parent = row.data('parent');
      var dependents = row.data('dependents');
      if (parent.length && dependents.length) {
        var dependentsChecked = false;
        $.each(dependents, function(){
          if ($$.isChecked(this)) {
            dependentsChecked = true;
          }
        });
        if (dependentsChecked) {
          parent.attr({'checked': true, 'disabled': true});
        }
        else {
          if (parent.not(checkbox).length) {
            parent.attr('checked', parent.data('parent-check-state'));
          }
          else {
            parent.data('parent-check-state', $$.isChecked(parent));
          }
          parent.removeAttr('disabled');
        }
      }
    },
    collapseFieldset: function(fieldset) {
      $('div.action', fieldset).hide();
        var content = $('> div:not(.action)', fieldset).slideUp('fast', function() {
        $(this.parentNode).addClass('collapsed');
        this.parentNode.animating = false;
      });
    },
    expandFieldset: function(fieldset) {
      if ($(fieldset).is('.collapsed')) {
        // Action div containers are processed separately because of a IE bug
        // that alters the default submit button behavior.
        var content = $('> div:not(.action)', fieldset);
        $(fieldset).removeClass('collapsed');
          content.hide();
          content.slideDown( {
          duration: 'fast',
          easing: 'linear',
          complete: function() {
            Drupal.collapseScrollIntoView(this.parentNode);
            this.parentNode.animating = false;
            $('div.action', fieldset).show();
          },
          step: function() {
            // Scroll the fieldset into view
            Drupal.collapseScrollIntoView(this.parentNode);
          }
        });
      }
    },
    permissionsType: function(element) {
      var checked = $(element).filter(':checked');
      var tbody = $$.closest('tbody', element);
      var permissions = $('input[name^="permissions"]', tbody);
      var fieldset = $('fieldset.form-fieldset');
      switch(checked.val()) {
        case 'all':
          plugin.settings.permissionState = 'all';
          permissions.each(function(){
            var previousCheckState = $(this).data('check-state');
            $(this).attr({'checked': true, 'disabled': 'disabled'});
            $(this).data('check-state', previousCheckState);
          });
          plugin.collapseFieldset(fieldset);
          break;
        case 'none':
          plugin.settings.permissionState = 'none';
          permissions.each(function(){
            var previousCheckState = $(this).data('check-state');
            $(this).removeAttr('checked').attr('disabled', 'disabled');
            $(this).data('check-state', previousCheckState);
          });
          plugin.collapseFieldset(fieldset);
          break;
        default:
          plugin.settings.permissionState = 'custom';
          permissions.each(function(){
            $(this).removeAttr('disabled');
            $(this).attr('checked', $(this).data('check-state'));
          });
          plugin.processDependents(tbody);
          plugin.expandFieldset(fieldset);
          break;
      }
    },
    processDependents: function(tbody) {
      $.each($('tr', tbody), function(){
        var row = $(this);
        var permissions = $('input', row);
        var parent = permissions.filter('[name^="permissions[navigate:"]');
        var dependents = permissions.filter(':not([name^="permissions[navigate:"])');
        row.data('parent', parent);
        parent.data('parent-check-state', $$.isChecked(parent));
        row.data('dependents', dependents);
        if (parent.length && dependents.length) {
          plugin.checkDependents(dependents.get(0));
        }
      });
    }
  });

})(Navigate.jQuery, Navigate);
