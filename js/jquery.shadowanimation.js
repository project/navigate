;(function($) {
 /**
  * Check whether the browser supports RGBA color mode.
  *
  * @author Mehdi Kabab <http://pioupioum.fr>
  * @type Boolean
  * @return True if the browser support RGBA. False otherwise.
  */
  function isRGBACapable() {
      var $script = $('script:first'),
          color   = $script.css('color'),
          result  = false;
      if (/^rgba/.test(color)) {
              result = true;
      } else {
          try {
              result = ( color != $script.css('color', 'rgba(0, 0, 0, 0.5)').css('color') );
              $script.css('color', color);
          } catch (e) {};
      }
      return result;
  };
  $.extend(true, $, {
    support: {
      'rgba': isRGBACapable()
    }
  });
  

 /*
  * Shadow animation jQuery-plugin
  * http://www.bitstorm.org/jquery/shadow-animation/
  * Copyright 2010 Edwin Martin <edwin@bitstorm.org>
  * Released under the MIT and GPL licenses.
  *
  * Modified to include RGBA
  * Copyright 2010 Mark Carver <mark.carver@me.com>
  * Released under the MIT and GPL licenses.
  */
	
  // function(i, attr) {
  //    $.fx.step[attr] = function(fx) {
  //      if (!fx.colorInit) {
  //        fx.start = getColor(fx.elem, attr);
  //        fx.end = getRGB(fx.end);
  //        fx.colorInit = true;
  //      }
  // 
  //      fx.elem.style[attr] = 'rgb(' +
  //        Math.max(Math.min(parseInt((fx.pos * (fx.end[0] - fx.start[0])) + fx.start[0], 10), 255), 0) + ',' +
  //        Math.max(Math.min(parseInt((fx.pos * (fx.end[1] - fx.start[1])) + fx.start[1], 10), 255), 0) + ',' +
  //        Math.max(Math.min(parseInt((fx.pos * (fx.end[2] - fx.start[2])) + fx.start[2], 10), 255), 0) + ')';
  //    };
  //   });
  

	// Extend the animate-function
	$.fx.step['shadow'] = function(fx) {
	  // First define which property to use
  	var boxShadow;
  	$.each(['boxShadow', 'MozBoxShadow', 'WebkitBoxShadow'], function(i, property) {
  		var val = $('body').css(property);
  		if (typeof val == 'string' && val != '') {
  			boxShadow = property;
  			return false;
  		}
  	});
    if (!fx.colorInit) {
			fx.start = parseShadow($(fx.elem).css(boxShadow));
			fx.end = parseShadow(fx.options.curAnim['shadow']);
			fx.colorInit = true;
		}
		fx.elem.style[boxShadow] = calculateShadow(fx.start, fx.end, fx.pos);
	};

	// Calculate an in-between shadow.
	function calculateShadow(start, end, pos) {
		var parts = [];
		if (typeof end.left != undefined) {
			parts.push(parseInt(start.left + pos * (end.left - start.left))+'px '
					+parseInt(start.top + pos * (end.top - start.top))+'px');
		};
		if (typeof end.blur != undefined) {
			parts.push(parseInt(start.blur + pos * (end.blur - start.blur))+'px');
		};
		if (typeof end.spread != undefined) {
			parts.push(parseInt(start.spread + pos * (end.spread - start.spread))+'px');
		};
		if (typeof end.color != undefined) {
		  if ($.support.rgba) {
		    if (typeof start.color[3] == undefined) start.color[3] = 1;
  		  if (typeof end.color[3] == undefined) end.color[3] = 1;
  		};
			parts.push('rgb'+($.support.rgba ? 'a' : '')+'('
				+ parseInt((start.color[0] + pos * (end.color[0] - start.color[0]))) + ','
				+ parseInt((start.color[1] + pos * (end.color[1] - start.color[1]))) + ','
				+ parseInt((start.color[2] + pos * (end.color[2] - start.color[2])))
				+ ($.support.rgba ? ',' + parseFloat(start.color[3] + pos * (end.color[3] - start.color[3])) : '' )
		    + ')'
		  );
		};
		if (start.inset) {
			parts.push('inset');
		};
    var value = parts.join(' ');
		return value;
	}

	// Parse the shadow value and extract the values.
	function parseShadow(shadow) {
		var match, color = [], parsedShadow = {};

		// Parse an CSS-syntax color. Outputs an array [r, g, b]
		// Match #aabbcc
		if (match = /#([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})/.exec(shadow)) {
			color = [parseInt(match[1], 16), parseInt(match[2], 16), parseInt(match[3], 16)];

			// Match #abc
		} else if (match = /#([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])/.exec(shadow)) {
			color = [parseInt(match[1], 16) * 17, parseInt(match[2], 16) * 17, parseInt(match[3], 16) * 17];

			// Match rgb(n, n, n)
		} else if (match = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(shadow)) {
			color = [parseInt(match[1]), parseInt(match[2]), parseInt(match[3])];

			// Match rgba(n, n, n, n)
		} else if (match = /rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-1]{1}?\.?[0-9]*)\s*\)/.exec(shadow)) {
			color = [parseInt(match[1]), parseInt(match[2]), parseInt(match[3]),parseFloat(match[4])];

			// No browser returns rgb(n%, n%, n%), so little reason to support this format.
		}
		if ($.support.rgba && !color[3]) color[3] = 1;

		// Parse offset, blur and radius
		if (match = /([0-9]+)(?:px)?\s+([0-9]+)(?:px)?(?:\s+([0-9]+)(?:px)?)?(?:\s+([0-9]+)(?:px)?)?/.exec(shadow)) {
			parsedShadow = {left: parseInt(match[1]), top: parseInt(match[2]), blur: match[3] ? parseInt(match[3]) : undefined, spread: match[4] ? parseInt(match[4]) : undefined};
		}

		// Inset or not
		parsedShadow.inset = /inset/.test(shadow);

		parsedShadow.color = color;

		return parsedShadow;
	}
})(Navigate.jQuery);

